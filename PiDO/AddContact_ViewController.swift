//
//  AddContact_ViewController.swift
//  PiDO
//
//  Created by sp developer on 12/7/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

//@objc protocol UpdateContacts {
//    func Contact_Updater(value : [Contact])
//}

class AddContact_ViewController: UIViewController, UIPopoverPresentationControllerDelegate,UITextFieldDelegate,Selected_Option {

    @IBOutlet weak var mFirstname: UITextField!
    @IBOutlet weak var mSurname: UITextField!
    @IBOutlet weak var mPhone: UITextField!
    @IBOutlet weak var mAccNo: UITextField!
    
    var ID : Int = 0
    var Firstname : String = ""
    var Surname : String = ""
    var PhoneNumber : String = ""
    var AccNumber : String = ""
    var Bank : String = ""
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == mPhone){
            
            let maxLength = 11
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
        
        else if(textField == mAccNo){
            
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
            
        }
        
        else {
            return true
        }

        
    }
    
    // dismiss keyboard on pressing the return key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }
    
    
    func pop(){
        performSegue(withIdentifier: "pop_addcontacts_banks", sender: self)
        
    }

    
    
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        banks_label_txt.text = "Access"
        
        print("Firstname = \(Firstname) \n Surname = \(Surname) \n PhoneNumber = \(PhoneNumber) \n AccNo = \(AccNumber) \n Bank = \(Bank)")
        
        if(Firstname != "" && Surname != ""){
            
            mFirstname.text = Firstname
            mSurname.text = Surname
            mPhone.text = PhoneNumber
            mAccNo.text = AccNumber
            banks_label_txt.text = Bank
            
            
        }
        
        
      
        
        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(AddContact_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
//        banks_label_txt.addTarget(self, action: "pop", forControlEvents: UIControlEvents.TouchDown)
//        

        
        
    }

    
    
    
    @IBOutlet weak var banks_label_txt: UITextField!
    
    @IBAction func Banks_Btn(_ sender: UIButton) {
        
   performSegue(withIdentifier: "pop_addcontacts_banks", sender: self)
        

    }

    func fromTableview(_ value: String) {
        
        banks_label_txt.text = value
    }

       
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == banks_label_txt){
            
            return false
        }
        else {
            return true
        }
    }
    
    
    // fuction to display alert to the UI
    func displayAlert(_ mTitle : String, msg : String) -> Void{
        let alertView : UIAlertView = UIAlertView()
        alertView.title = mTitle
        alertView.message = msg
        alertView.delegate = self
        alertView.addButton(withTitle: "OK")
        alertView.show()
    }
    
    
    
    
    @IBAction func updateContact_Btn(_ sender: UIButton) {
        
        let Firstname = mFirstname.text! as NSString
        let Surname = mSurname.text! as NSString
        let PhoneNumber = mPhone.text! as NSString
        let AccNumber = mAccNo.text! as NSString
        let Bank = banks_label_txt.text! as NSString
        
        mFirstname.resignFirstResponder()
        mSurname.resignFirstResponder()
        mPhone.resignFirstResponder()
        mAccNo.resignFirstResponder()
        
      
        if(mFirstname.text!.isEmpty && mSurname.text!.isEmpty && mPhone.text!.isEmpty && mAccNo.text!.isEmpty ){
            
            displayAlert("ERROR!",msg: "Insert The Required Details Into Text Fields")
            
        }
            
        else if(Firstname.length == 0){
            
            displayAlert("ERROR!",msg: "Firstname is required")
            
        }
            
        else if(Surname.length == 0){
            
            displayAlert("ERROR!",msg: "Surname Is Required")
            
        }
            
        else if(PhoneNumber.length == 0){
            
            displayAlert("ERROR!",msg: "Phone Number Is Required")
            
        }
            
        else if(AccNumber.length == 0){
            displayAlert("ERROR!",msg: "Account Number Is Required")
        }
            
        else if(PhoneNumber.length < 11){
            
            displayAlert("ERROR!",msg: "11 Digit Phone Number Is Required")
            
        }
            
        else if(AccNumber.length < 10){
            displayAlert("ERROR!",msg: "10 Digit Account Number Is Required")
            
        }
            
        else {
            
            
            let contact_ = Contact()
            contact_.setFirstname(Firstname as String)
            contact_.setSurname(Surname as String)
            contact_.setBank(Bank as String)
            contact_.setPhoneNumber(PhoneNumber as String)
            contact_.setAccountNumber(AccNumber as String)
            
            
            let db_contact = Contacts_DB()
            
            db_contact.createTable()
            db_contact.updateContact(contact_, id: ID)
            
            
            
            let controller2 = UIAlertController(title: "Update Successful!", message: "Contact Update was Successful..", preferredStyle: UIAlertControllerStyle.alert)
            
            let ActionButton = UIAlertAction(title: "OK",style: .default, handler: { action in
                
                self.performSegue(withIdentifier: "unwind_to_contacts", sender: self)

                
            })
            
            controller2.addAction(ActionButton)
            self.present(controller2, animated: true, completion: nil)
            
        }
        
    }
    
    
    @IBAction func AddContact_Btn(_ sender: UIButton) {
        
        let Firstname = mFirstname.text! as NSString
        let Surname = mSurname.text! as NSString
        let PhoneNumber = mPhone.text! as NSString
        let AccNumber = mAccNo.text! as NSString
        let Bank = banks_label_txt.text! as NSString
        
        mFirstname.resignFirstResponder()
        mSurname.resignFirstResponder()
        mPhone.resignFirstResponder()
        mAccNo.resignFirstResponder()
        

        
        if(mFirstname.text!.isEmpty && mSurname.text!.isEmpty && mPhone.text!.isEmpty && mAccNo.text!.isEmpty ){
            
              displayAlert("ERROR!",msg: "Insert The Required Details Into Text Fields")
            
        }
        
        else if(Firstname.length == 0){
            
            displayAlert("ERROR!",msg: "Firstname is required")
            
        }
        
        else if(Surname.length == 0){
            
            displayAlert("ERROR!",msg: "Surname Is Required")
            
        }
        
        else if(PhoneNumber.length == 0){
            
            displayAlert("ERROR!",msg: "Phone Number Is Required")
            
        }
        
        else if(AccNumber.length == 0){
            displayAlert("ERROR!",msg: "Account Number Is Required")
        }
        
        else if(PhoneNumber.length < 11){
            
            displayAlert("ERROR!",msg: "11 Digit Phone Number Is Required")
            
        }
        
        else if(AccNumber.length < 10){
            displayAlert("ERROR!",msg: "10 Digit Account Number Is Required")
            
        }
        
        else {
            
            let contact_ = Contact()
            contact_.setFirstname(Firstname as String)
            contact_.setSurname(Surname as String)
            contact_.setBank(Bank as String)
            contact_.setPhoneNumber(PhoneNumber as String)
            contact_.setAccountNumber(AccNumber as String)
            
            
            let db_contact = Contacts_DB()
            
            db_contact.createTable()
            db_contact.saveContact(contact_)
            
            let length = Contacts_DB().getAll().count
            
            let controller2 = UIAlertController(title: "Contact Added!", message: "Contact Added Successfully..", preferredStyle: UIAlertControllerStyle.alert)
            
            let ActionButton = UIAlertAction(title: "OK",style: .destructive, handler: { action in
                
               
                
                if length == 1 {
                    
                    self.performSegue(withIdentifier: "unwind_main_menu", sender: self)
                }
                    
                else {
                    
                    self.performSegue(withIdentifier: "unwind_to_contacts", sender: self)
                }
                
            })
            
            
            
            controller2.addAction(ActionButton)
            self.present(controller2, animated: true, completion: nil)
            
            
        }
       
        
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let identifier = segue.identifier {
            
            switch identifier {
                
                case "pop_addcontacts_banks":
                    if let vc = segue.destination as? Options_TableViewController {
                        
                        vc.table_items = ["Access","Citi","Diamond","Ecobank","Enterprise",
                            "FCMB","Fidelity","FirstBank","GTB",
                            "Heritage","KeyStone","MainStreet",
                            "Skye","Stanbic","Standard","Sterling",
                            "UBA","Union","Unity","Wema","Zenith"]
                        
                            vc.delegate =  self
                            
                        }
                
                
            default:
                break
            }
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        
        return UIModalPresentationStyle.none
    }
    

}
