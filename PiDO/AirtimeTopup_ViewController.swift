//
//  AirtimeTopup_ViewController.swift
//  PiDO
//
//  Created by sp developer on 11/21/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

class AirtimeTopup_ViewController: UIViewController, UIPopoverPresentationControllerDelegate, Selected_ValueDelegate,SelectAmount,Selected_Option,Selected_Contact,UITextFieldDelegate {
   
    @IBOutlet weak var mPIN: UITextField!
    @IBOutlet weak var mRecepientNo: UITextField!
    
    var indicator : ActivityIndicator!
    var Recipient : NSString = ""
    var db_MobileNo = ""
    var PIN : NSString!
    var Amount : NSString!
    var Network : NSString!
    
    var Contacts_DB_status : Bool = false
    
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
        
//        indicator.stopAnimating()
//        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }
    
    func pop_amount(){
        performSegue(withIdentifier: "pop_amounts", sender: self)
        
    }
    
    func pop_network(){
        performSegue(withIdentifier: "pop_networks", sender: self)
   
    }
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        label_network.text = "AIRTEL"
        amount_label.text = "100"

        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(AirtimeTopup_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
        
      //  label_network.addTarget(self, action: "pop_network", forControlEvents: UIControlEvents.TouchDown)
      //  amount_label.addTarget(self, action: "pop_amount", forControlEvents: UIControlEvents.TouchDown)
        
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            
            
        }
        
        
        if(Recipient.length > 0){
            
            mRecepientNo.text = Recipient as String
        }
        
    }
    
    func fromTableview(_ value: String) {
        
        if(value == "9Mobile"){
            amount_label.text = "50"
            label_network.text =  value
            
        }
            
        else{
            
            label_network.text =  value
            amount_label.text = "100"
            
            
        }

    }
    
    func AmountsTableview(_ value: String) {
        
        amount_label.text = value
        
    }
    
    func From_GetPidoContactsTable(_ value: String) {
        mRecepientNo.text = value
    }

    
    @IBOutlet weak var label_network: UITextField!
    
    func fromPOPover(_ value:String){
        
        if(value == "9Mobile"){
            amount_label.text = "50"
            label_network.text =  value
            
        }
        
        else{
            
            label_network.text =  value
            amount_label.text = "100"
            
            
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if(textField == label_network){
            
            return false
            
        }
        
        else if(textField == amount_label){
            return false
        }
        
        else{
            return true
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }
    
    // delegate method to limit the range of characters in each text field
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == mPIN{
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength

        }
        
        else if(textField == mRecepientNo){
            
            let maxLength = 11
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            

            
        }
        
        return false
    }
    
    // delegate function to update popver selection to the textfield
    func fromAmountPopover(_ value:String){
        amount_label.text = value
    }
    
    // fuction to display alert to the UI
    func displayAlert(_ mTitle : String, msg : String) -> Void{
        let alertView : UIAlertView = UIAlertView()
        alertView.title = mTitle
        alertView.message = msg
        alertView.delegate = self
        alertView.addButton(withTitle: "OK")
        alertView.show()
    }
    
    func Airtime_confirmation_Alert(_ no_ : NSString, network:NSString , amount : NSString ){
       
        let msg = "Recipient Phone No : \(no_)\n\nNetwork : \(network)\n\nAmount : ₦\(amount)\n\nTransaction Charge : ₦0\n\nTotal : ₦\(amount)"
        
        //
        let controller2 = UIAlertController(title: "Transaction Details", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Dismiss" , style: UIAlertActionStyle.cancel, handler: nil)
        let ActionButton = UIAlertAction(title: "Proceed",style: .destructive, handler: {action in
            
            let encodedPIN = UtilHelpers().toBase64(self.PIN as String)
            let encodedRecipient = UtilHelpers().toBase64(self.Recipient as String)
            let encodedMsisdn = UtilHelpers().toBase64(self.db_MobileNo)
        
            let airtime_url = Constants.BASE_URL+"method=airtime&msisdn=\(encodedMsisdn)&pin=\(encodedPIN)&network=\(self.Network as String)&amount=\(self.Amount as String)&recipientmobile=\(encodedRecipient)"
            
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                self.AirtimeTop_up(airtime_url)
                
            }
            
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
        
        })
        controller2.addAction(cancelAction)
        controller2.addAction(ActionButton)
        self.present(controller2, animated: true, completion: nil)
        
    }
    
    

    @IBAction func mNetwork(_ sender: UIButton) {
        
        
        performSegue(withIdentifier: "pop_networks", sender: self)
    }
    
    func AirtimeTop_up(_ url_ : String) -> Void{
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let safeURL = url_.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let request = NSMutableURLRequest(url: URL(string: safeURL!)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest) {Data, response, error -> Void in
            
            DispatchQueue.main.async {
                
                if(Data!.count < 0){
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    self.indicator.stopAnimating()
                    //UIApplication.sharedApplication().beginIgnoringInteractionEvents()
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    self.mRecepientNo.text = ""
                    self.mPIN.text = ""
                    
                }
                    
                else{
                    print("\(Data!.count) bytes of data was returned")
                    responseStr  = NSString(data: Data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    self.indicator.stopAnimating()
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    self.mRecepientNo.text = ""
                    self.mPIN.text = ""
                    
                    
                }
            }
            
            
            
        }
        
        
       task.resume()

        
    }
    
    
    @IBAction func getPidoContacts_Btn(_ sender: UIButton) {
        
        Contacts_DB_status = Contacts_DB().isExisting()
        
        
        if(Contacts_DB_status == false) {
            
            displayAlert("No Content",msg: "Your PiDO Contact list is empty")
        }
            
        else {
           performSegue(withIdentifier: "get_contacts", sender: self)
            
        }
        
    }
    
    @IBOutlet weak var amount_label: UITextField!
    
    
    @IBAction func Amount_Btn(_ sender: UIButton) {
        performSegue(withIdentifier: "pop_amounts", sender: self)
    }
    
    
    @IBAction func Airtime_Btn(_ sender: UIButton) {
        
        mRecepientNo.resignFirstResponder()
        mPIN.resignFirstResponder()
        
         Recipient = mRecepientNo.text! as NSString
         PIN = mPIN.text! as NSString
         Amount = amount_label.text! as NSString
         Network = label_network.text! as NSString
        
        if mRecepientNo.text!.isEmpty && mPIN.text!.isEmpty {
            
            displayAlert("ERROR!",msg: "Recipient number and PIN is required")
            
        }
            
        else if(Recipient.length == 0){
            
            displayAlert("ERROR!",msg: "Recipient number is requried")
            
        }
        
        else if(Recipient.length < 11){
            displayAlert("ERROR!",msg: "11 Digit Recipient Number Required")
            
        }
            
        else if(PIN.length == 0){
            
             displayAlert("ERROR!",msg: "4 Digit PIN Required")
        }
        
        else if(PIN.length < 4){
            
            displayAlert("ERROR!",msg: "4 Digit PIN Required")
            
        }
        
        else {
            Airtime_confirmation_Alert(Recipient, network: Network, amount: Amount)
            print("Ready!!")
            
        }
        
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let identifier = segue.identifier {
            
            switch identifier {
                
                case "pop_networks":
                    if let vc = segue.destination as? Options_TableViewController {
                        
                        vc.table_items = ["AIRTEL","9Mobile","GLO","MTN"]
                        vc.delegate = self
                    }
//                    if let vc = segue.destinationViewController as? NetworkPopover_ViewController{
//                        if let ppc = vc.popoverPresentationController{
//                            vc.preferredContentSize = CGSizeMake(350, 150)
//                            vc.delegate = self
//                             ppc.delegate = self
//                        }
//                        
//                    }
                case "get_contacts":
                    if let vc = segue.destination as? GetPidoContacts_TableTableViewController{
                     
                        vc.delegate =  self
                    }
                
                
                case "pop_amounts":
                    
                    if let vc = segue.destination as? Amounts_TableViewController {
                      
                        if (label_network.text == "AIRTEL") {
                            
                            vc.amountItems = ["100","200","500","1000"]
                        }
                        
                        else if(label_network.text == "9Mobile"){
                            
                            vc.amountItems = ["50","100","200","500","1000"]

                        }
                        
                        else if(label_network.text == "GLO"){
                            
                            vc.amountItems = ["100","150","200","300","500","1000","3000"]
                        }
                        
                        else if(label_network.text == "MTN"){
                            
                            vc.amountItems = ["100","200","400","750","1500","3000"]
                        }
                        
                        
                        vc.delegate = self
                    }
                
                
            default:
                break
            }
            
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        
        return UIModalPresentationStyle.none
    }
    

}
