//
//  Alert.swift
//  PiDO
//
//  Created by sp developer on 12/22/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import Foundation
import UIKit

// contain helper methods
class UtilHelpers {
    
    // method to display UIAlert
      func displayAlert(_ mTitle : String, msg : String) -> Void{
            let alertView : UIAlertView = UIAlertView()
            alertView.title = mTitle
            alertView.message = msg
            alertView.delegate = self
            alertView.addButton(withTitle: "OK")
            alertView.show()
        }
    
    // method to encode to base64
    func toBase64(_ PlainString : String) -> String {
        let plainData = (PlainString as NSString).data(using: String.Encoding.utf8.rawValue)
        
        let base64String = plainData!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        return base64String
    }
    
}
