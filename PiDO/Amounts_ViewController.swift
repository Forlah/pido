//
//  Amounts_ViewController.swift
//  PiDO
//
//  Created by sp developer on 11/24/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit
@objc protocol Selected_AmountDelegate {
    func fromAmountPopover(_ value:String)
}

class Amounts_ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var visibleNetworkName:String = "Airtel"
    var displayedRow : String = ""
    var out : String = "100"
    let Airtel = ["100","200","500","1000"]
    let Etisalat = ["50","100","200","500","1000"]
    let GLO = ["100","150","200","300","500","1000","3000"]
    let MTN = ["100","200","400","750","1500","3000"]
    
    
    weak var delegate: Selected_AmountDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       _ = picker.selectedRow(inComponent: 0)
    
        if(visibleNetworkName == "ETISALAT"){
            out = "50"
        }
       delegate?.fromAmountPopover(out)
        
    }

    @IBOutlet weak var picker: UIPickerView!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if visibleNetworkName == "AIRTEL" {
            
            return Airtel.count
        }
        
        else if(visibleNetworkName == "ETISALAT"){
            
            return Etisalat.count
        }
        
        else if visibleNetworkName == "GLO" {
            
            return GLO.count
        }
        
        else {
            return MTN.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        
        if visibleNetworkName == "MTN" {
            
            displayedRow = "MTN"
            return MTN[row]
        }
            
        else if(visibleNetworkName == "ETISALAT"){
            
            displayedRow = "ETISALAT"
            return Etisalat[row]
        }
            
        else if visibleNetworkName == "GLO" {
            displayedRow = "GLO"

            return GLO[row]
        }
            
        else  {
            
            displayedRow = "AIRTEL"
            return Airtel[row]
        }

    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let row = picker.selectedRow(inComponent: 0)
        
        if displayedRow == "AIRTEL" {
            out = Airtel[row]
            delegate?.fromAmountPopover(Airtel[row])
        }
        
        else if displayedRow == "ETISALAT" {
            out = Etisalat[row]
            delegate?.fromAmountPopover(Etisalat[row])
        }
        else if displayedRow == "GLO" {
            out = GLO[row]
            delegate?.fromAmountPopover(GLO[row])
            
        }
        else if displayedRow == "MTN" {
                 out =  MTN[row]
               delegate?.fromAmountPopover(MTN[row])
        }

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
