//
//  Araya_ViewController.swift
//  PiDO
//
//  Created by sp developer on 1/15/16.
//  Copyright (c) 2016 sp developer. All rights reserved.
//

import UIKit

class Araya_ViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var mEnrolleeID: UITextField!
    @IBOutlet weak var mPIN: UITextField!
    var indicator : ActivityIndicator!
    
    
    var EnrolleeID : NSString = ""
    var PIN : NSString = ""
    
    var db_MobileNo = ""
    var resultString : NSString = ""
    
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            
            
        }
        
        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(Araya_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
    }
    
    
    func Araya_task(url_: String){
        
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){data, response, error in
            
            if let error = error {
                print( (error))
                
                responseStr = "Unable to connect to the service at the moment, Please try again later"
                self.indicator.stopAnimating()
                
                UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
            }
                
            else {
                
                print("\(data!.count) bytes of data was returned")
                
                responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                self.indicator.stopAnimating()
                
                UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                
                self.mEnrolleeID.text = ""
                self.mPIN.text = ""

                
            }
            
        }
        task.resume()
        
    }
    
    
    func verify_task(url_: String){
        
        var name : NSString = ""
        var plan : NSString = ""
        var araya_amount : NSString = ""
        var trans_charge : NSString = ""
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){data, response, error in
            
            if let error = error {
                print( (error))
                
                responseStr = "Unable to connect to the service at the moment, Please try again later"
                self.indicator.stopAnimating()
                
                UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
            }
                
            else {
                
                print("\(data!.count) bytes of data was returned")
                
                responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                self.indicator.stopAnimating()
                
                if(responseStr.hasPrefix("|0||400")) { // must return a count of 4, o
                    
                    print("here in 400")
                    UtilHelpers().displayAlert("FAILED", msg: "Please Check Your Enrollee ID")
                    
                }
                    
                else if(responseStr.contains("|")){
                    
                    
                    let seperated_String = responseStr.components(separatedBy: "|")
                    
                    name = seperated_String[0] as NSString
                    araya_amount = seperated_String[1] as NSString
                    plan = seperated_String[2] as NSString
                    trans_charge = seperated_String[3] as NSString
                    
                    
                    if(name.contains("200") ){
                        
                        name = "Unavailable"
                    }
                        
                    else if(araya_amount.contains("200")){
                        
                        araya_amount = "Unavailable";
                        
                    }
                        
                    else if(plan.contains("200")){
                        
                        plan = "Unavailable";
                        
                    }
                        
                    else if(trans_charge.contains("200") || trans_charge.contains("400")){
                        
                        trans_charge = "Unavailable";
                    }
                        
                    else {
                        
                        self.indicator.stopAnimating() // dismiss indicator
                        
                        
                        // display a dialog for the user to verify the acc
                        let total = (trans_charge as NSString).integerValue + (araya_amount as NSString).integerValue
                        
                        let msg = "Name : \(name)\n\nEnrolleeID : \(self.EnrolleeID)\n\nPlan : \(plan)\n\nTransaction Charge : ₦\(trans_charge)\n\nAmount : ₦\(araya_amount)\n\nTotal : ₦\(total)"
                        
                        let controller = UIAlertController(title: "Transaction Details", message: msg, preferredStyle: UIAlertControllerStyle.alert)
                        let cancelAction = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil)
                        let ActionButton = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.destructive, handler: { action in
                            
                            let araya_url = Constants.BASE_URL+"method=arayapayment&enrolleeid=\(self.EnrolleeID)&mobile=\(self.db_MobileNo)&amount=\(araya_amount)&pin=\(self.PIN)&productid=38"
                            
                            
                            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                                
                                self.Araya_task(url_: araya_url)
                            }
                                
                            else {
                                
                                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                                
                            }
                            
                            
                            
                            
                        })
                        
                        
                        controller.addAction(cancelAction)
                        controller.addAction(ActionButton)
                        self.present(controller, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                    
                    
                    
                else {
                    
                    
                    UtilHelpers().displayAlert("Response", msg: self.resultString as String)
                    
                }

                
            }
            

            
        }
        task.resume()
        
        
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == mEnrolleeID {
            
            let maxLength = 14
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
            
            
        }
            
        else {
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
        
        
    }
    
    
    @IBAction func Araya_Btn(_ sender: UIButton) {
        
        mEnrolleeID.resignFirstResponder()
        mPIN.resignFirstResponder()
        
        EnrolleeID = mEnrolleeID.text! as NSString
        PIN = mPIN.text! as NSString
        
        if(mEnrolleeID.text!.isEmpty && mPIN.text!.isEmpty){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Insert the required details into text fields")
            
        }
            
        else if(EnrolleeID.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Araya Enrollee ID Is Required")
            
        }
            
        else if(EnrolleeID.length < 14){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Invalid Araya Enrollee ID")
            
        }
            
        else if(PIN.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "4 Digit PIN Required")
            
        }
            
        else if(PIN.length < 4){
            
            UtilHelpers().displayAlert("ERROR!", msg: "4 Digit PIN Required")
            
        }
            
            
        else {
            
            let araya_enquiry_url = Constants.BASE_URL+"method=arayaenquiry&enrolleeid=\(EnrolleeID)&mobile=\(db_MobileNo)"
            
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                verify_task(url_: araya_enquiry_url)
                
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
            
        }
        
        
    }

}
