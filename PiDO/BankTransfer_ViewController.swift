//
//  BankTransfer_ViewController.swift
//  PiDO
//
//  Created by sp developer on 11/25/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

class BankTransfer_ViewController: UIViewController, UIPopoverPresentationControllerDelegate , Selected_Option,Selected_Contact,UITextFieldDelegate{
    
    var indicator : ActivityIndicator!
    var AccNumber : NSString = ""
    var Amount : NSString = ""
    var Recipient : NSString = ""
    var PIN : NSString = ""
    var Remark : NSString = ""
    var bank_name : String = ""
    var db_MobileNo = ""
    
    var Contacts_DB_status : Bool = false
    


    @IBOutlet weak var mAccountNumber: UITextField!
    @IBOutlet weak var mAmount: UITextField!
    @IBOutlet weak var mRecipient: UITextField!
    @IBOutlet weak var mRemark: UITextView!
    @IBOutlet weak var mPIN: UITextField!
   
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
    
    func fromTableview(_ value: String) {
        
        Bank_label.text = value
        
    }
    
      override func viewDidLoad() {
        super.viewDidLoad()
        
         Bank_label.text! = "Access"
        
        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(BankTransfer_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
     //    Bank_label.addTarget(self, action: "pop", forControlEvents: UIControlEvents.TouchDown)
        
        if(Recipient.length != 0 && AccNumber.length != 0){
            
            Bank_label.text = bank_name
            mRecipient.text = Recipient as String
            mAccountNumber.text = AccNumber as String
        }
        
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            
        }
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == Bank_label {
            return false
        }
        else {
            return true
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == mAccountNumber {
            
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
        
        else if textField == mRecipient {
            
            let maxLength = 11
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
            
        }
        
        else if textField == mPIN {
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
            
        }
        
        else {
            
            return true
            
        }
        
        
    }
    
    
    @IBOutlet weak var Bank_label: UITextField!
    
    
    
    func fromBanksPopover(_ value:String){
        Bank_label.text = value
    }
    
    
    func From_GetPidoContactsTable(_ value: String) {
        mRecipient.text = value
    }
    
    
    func getNameEnquiry_charge(url_: String){
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async { // main thread
                
                if let error = error {
                    
                    print("\(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    self.indicator.stopAnimating()
                    
                    
                    if (responseStr.contains("|")){
                        var Acc_Name : String
                        
                        let seperated_String = responseStr.components(separatedBy: "|")
                        
                        let charge  = seperated_String[0]
                        
                        
                        if(seperated_String[1].isEqual("200")){
                            Acc_Name = "Unavailable"
                        }
                            
                        else {
                            
                            Acc_Name = seperated_String[1]
                        }
                        
                        // display a dialog for the user to verify the accname, bank etc
                        
                        self.BankTransfer_Confirmation_Alert(charge, acc_name: Acc_Name)
                        
                        
                    }
                        
                        
                    else {
                        
                        UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                        
                    }
                    
                }

                
            }
            
            
        }
        task.resume()
        
    
    }
    
    func BankTransfer_Task(url_: String){
        
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let safeURL = url_.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let request = NSMutableURLRequest(url: URL(string: safeURL!)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
             DispatchQueue.main.async { // main thread  
                
                if let error = error {
                    
                    print("\(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    self.indicator.stopAnimating() // dismiss activity indicator
                    
                    print("Response from service : \(responseStr)")
                    
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String) // display the response from the service
                    
                    
                    self.mAccountNumber.text = ""
                    self.mAmount.text = ""
                    self.mRecipient.text = ""
                    self.mPIN.text = ""
                    self.mRemark.text = ""
                }
                
                
             }
            
        }
        task.resume()
        
    }
    
    
    
    func BankTransfer_Confirmation_Alert(_ charge : String , acc_name : String) {
        
        let total = (charge as NSString).integerValue + Amount.integerValue
        
        let msg = "Account Name : \(acc_name)\n\nAccount Number : \(AccNumber)\n\nBank : \(bank_name)\n\nRecepient Number : \(Recipient)\n\nAmount : ₦\(Amount)\n\nTranscation charge : ₦\(charge)\n\nTotal : ₦\(total)"
        
        let controller2 = UIAlertController(title: "Transaction Details", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Dismiss" , style: UIAlertActionStyle.cancel, handler: nil)
        let ActionButton = UIAlertAction(title: "Proceed",style: .destructive, handler: {action in
            
            print("The proceed button was pressed")
            
            
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                let encodedMobileNo = UtilHelpers().toBase64(self.db_MobileNo)
                let encodedPIN = UtilHelpers().toBase64(self.PIN as String)
                
                
               let transfer_task_uri = Constants.BASE_URL+"method=pidotobank&amount=\(self.Amount)&bankname=\(self.bank_name)&mobile=\(encodedMobileNo)&remark=\(self.Remark)&nubanacc=\(self.AccNumber)&pin=\(encodedPIN)&recipientmobile=\(self.Recipient)"
                
                
                self.BankTransfer_Task(url_: transfer_task_uri)
                
                
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
            
        })
        
        
        controller2.addAction(cancelAction)
        controller2.addAction(ActionButton)
        self.present(controller2, animated: true, completion: nil)
        
        
    }
    
    @IBAction func SelectBank_Btn(_ sender: UIButton) {
        
        performSegue(withIdentifier: "pop_banks", sender: self)
        
    }

    
    @IBAction func GetPidoContact_Btn(_ sender: UIButton) {
        
            Contacts_DB_status = Contacts_DB().isExisting()
            
            
            if(Contacts_DB_status == false) {
                
                 UtilHelpers().displayAlert("No Content",msg: "Your PiDO Contact list is empty")
            }
                
            else {
                performSegue(withIdentifier: "get_contacts", sender: self)
                
            }
            
        

        
    }
    
    @IBAction func BankTransfer_Btn(_ sender: UIButton) {
        
        // dismiss any textfield that is a presently the first responder
        
        mAccountNumber.resignFirstResponder()
        mAmount.resignFirstResponder()
        mRecipient.resignFirstResponder()
        mPIN.resignFirstResponder()
        mRemark.resignFirstResponder()
        
        
         AccNumber = mAccountNumber.text! as NSString
         Amount = mAmount.text! as NSString
         Recipient = mRecipient.text! as NSString
         PIN = mPIN.text! as NSString
         Remark = mRemark.text as NSString
         bank_name = Bank_label.text!
        
        
        
        if (mAccountNumber.text!.isEmpty && mAmount.text!.isEmpty && mRecipient.text!.isEmpty && mRemark.text.isEmpty && mPIN.text!.isEmpty){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Insert the required details into text fields ")
            
        }
        
        else if AccNumber.length == 0 || AccNumber.length < 10{
            
            UtilHelpers().displayAlert("ERROR!", msg: "10 Digit NUBAN Account Number Required ")
            
        }
            
        else if Amount.length == 0 {
            UtilHelpers().displayAlert("ERROR!", msg: "Amount Text Field Required")
            
        }
        
        else if Recipient.length == 0 || Recipient.length < 11 {
            
            UtilHelpers().displayAlert("ERROR!", msg: "11 Digit Recipient Phone Number Required ")
            
        }
        
        else if PIN.length == 0 || PIN.length < 4{
            UtilHelpers().displayAlert("ERROR!", msg:"4 Digit PIN Required")
        }
        
        else {
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                let encodedAcc_No = UtilHelpers().toBase64(self.AccNumber as String)
                
                
               let verify_url = Constants.BASE_URL+"method=getchargeandbankname&amount=\(Amount)&nubanacc=\(encodedAcc_No)&bankname=\(bank_name)&mobile=\(Recipient)"
                
                   getNameEnquiry_charge(url_: verify_url)
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
        }
        
    }
    
  // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let identifier = segue.identifier {
            
            switch identifier {
                
                case "pop_banks":
                    if let vc = segue.destination as? Options_TableViewController {
                        
                        vc.table_items = ["Access","Citi","Diamond","Ecobank","Enterprise",
                            "FCMB","Fidelity","FirstBank","GTB",
                            "Heritage","KeyStone","MainStreet",
                            "Skye","Stanbic","Standard","Sterling",
                            "UBA","Union","Unity","Wema","Zenith"]
                        
                        vc.delegate = self
                     }
                
                case "get_contacts":
                    if let vc = segue.destination as? GetPidoContacts_TableTableViewController{
                        vc.delegate = self
                    }
                
            default:
                break
            }
        }
        
    }
    
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        
        return UIModalPresentationStyle.none
    }

}
