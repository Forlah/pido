//
//  Banks_list _ViewController.swift
//  PiDO
//
//  Created by sp developer on 11/30/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

@objc protocol Selected_Bank_Delegate {
    func fromBanksPopover(_ value: String)
}

class Banks_list__ViewController: UITableViewController {

    var db_Contacts : [Contact] = []
    var length : Int = 1
    var rowContact =  Contact()
    var items : [String] = []
    
    
    
        
    weak var delegate : Selected_Bank_Delegate?
    var visibleBank = "Access"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        db_Contacts = Contacts_DB().getAll()
        
        

    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return db_Contacts.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactslist_cell", for: indexPath)
        
        let surname = db_Contacts[indexPath.row].getSurname()
        let firstname = db_Contacts[indexPath.row].getFirstname()
        let fullname = surname + firstname
        cell.textLabel?.text = fullname
        
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let row = indexPath.row
//        let rowData = amountItems[row]
//        
//        delegate?.AmountsTableview(rowData)
//        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
