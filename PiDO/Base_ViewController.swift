//
//  Base_ViewController.swift
//  PiDO
//
//  Created by sp developer on 1/11/16.
//  Copyright (c) 2016 sp developer. All rights reserved.
//

import UIKit

class Base_ViewController: UIViewController {

   
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(true)
        
        // check if database containing pido user credentials is existing
        
        if(PiDOUser_DB().isExisting() ==  true){
            performSegue(withIdentifier: "to login", sender: self)
        }
            
        else {
            performSegue(withIdentifier: "to registration", sender: self)
        }

    }
    

   

}
