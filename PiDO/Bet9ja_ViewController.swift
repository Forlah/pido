//
//  Bet9ja_ViewController.swift
//  PiDO
//
//  Created by sp developer on 12/14/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

class Bet9ja_ViewController: UIViewController,UIPopoverPresentationControllerDelegate,Selected_Option , UITextFieldDelegate{
    
    var indicator : ActivityIndicator!
    
    var types : NSString = ""
    var CustomerID : NSString = ""
    var Amount : NSString = ""
    var PIN : NSString = ""
    
    var db_MobileNo = ""


    @IBOutlet weak var types_label: UITextField!
    
    @IBOutlet weak var mCustomerID: UITextField!
    
    @IBOutlet weak var mAmount: UITextField!
    @IBOutlet weak var mPIN: UITextField!
    
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
    
    func fromTableview(_ value: String) {
        
        types_label.text = value
    }


    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        types_label.text = "Agent"
        
        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(Bet9ja_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
       // types_label.addTarget(self, action: "pop", forControlEvents: UIControlEvents.TouchDown)
        
        
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            
        }

        
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == types_label) {
            return false
        }
        
        else {
            return true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == mPIN){
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
            
                   
        else {
            return true
        }
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }

    

    @IBAction func Types_Btn(_ sender: UIButton) {
        performSegue(withIdentifier: "pop_bet9ja", sender: self)
        
    }
    
    func Bet9ja_Task(url_: String){
        
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let safeURL = url_.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let request = NSMutableURLRequest(url: URL(string: safeURL!)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    print("\(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    self.indicator.stopAnimating() // dismiss activity indicator
                    
                    print("Response from service : \(responseStr)")
                    
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String) // display the response from the service
                    
                    
                    self.mCustomerID.text! = ""
                    self.mAmount.text! = ""
                    self.mPIN.text! = ""
                    
                }
                
                
            }
            
            
        }
        task.resume()
            
        
        
    }
    
    func Bet9ja_Confirmation_Alert(_ acc_name : String) {
        
        //let total = (charge as NSString).integerValue + Amount.integerValue
        
        let msg = "Account Name : \(acc_name)\n\nBet9ja ID : \(CustomerID)\n\nType : \(types)\n\nAmount : ₦\(Amount)\n\ncharge : ₦0\n\nTotal : ₦\(Amount)"
        
        let controller2 = UIAlertController(title: "Transaction Details", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Dismiss" , style: UIAlertActionStyle.cancel, handler: nil)
        let ActionButton = UIAlertAction(title: "Proceed",style: .destructive, handler: {action in
            
            print("The proceed button was pressed")
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                let encodedMobileNo = UtilHelpers().toBase64(self.db_MobileNo)
                let encodedPIN = UtilHelpers().toBase64(self.PIN as String)
                
                let bet9ja_url = Constants.BASE_URL+"method=betnaijafundwallet&amount=\(self.Amount)&customerid=\(self.CustomerID as String)&customermobile=\(encodedMobileNo)&pin=\(encodedPIN)&typeofoperation=\(self.types as String)"
            
                
                self.Bet9ja_Task(url_: bet9ja_url)
                
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
        })
        
        
        controller2.addAction(cancelAction)
        controller2.addAction(ActionButton)
        self.present(controller2, animated: true, completion: nil)
        
        
    }
    
    
    func getNameEnquiry(url_: String){
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        var responseStr: NSString = ""
        let safeURL = url_.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let request = NSMutableURLRequest(url: URL(string: safeURL!)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    print("\(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    self.indicator.stopAnimating()
                    
                    
                    let result = responseStr
                    
                    // display a dialog for the user to verify the accname, bank etc
                    
                    self.Bet9ja_Confirmation_Alert(result as String)
                    
                    
                }
                
            }
            
            
            
            
        }
        task.resume()
        
    }
    
   
    @IBAction func Bet9ja_Btn(_ sender: UIButton) {
        
        // dismiss any textfield that is a first responder
        
        mCustomerID.resignFirstResponder()
        mAmount.resignFirstResponder()
        mPIN.resignFirstResponder()
        
        types = types_label.text! as NSString
        CustomerID = mCustomerID.text! as NSString
        Amount = mAmount.text! as NSString
        PIN = mPIN.text! as NSString
        
        if(mCustomerID.text!.isEmpty && mAmount.text!.isEmpty && mPIN.text!.isEmpty){
            
               UtilHelpers().displayAlert("ERROR!", msg: "Insert the required details into text fields ")
            
        }
        
        else if(CustomerID.length == 0){
            UtilHelpers().displayAlert("ERROR!", msg: "Bet9ja ID Required")
        }
        
        else if(Amount.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Amount Text Field Required")
        }
        
        else if PIN.length == 0 || PIN.length < 4 {
            
             UtilHelpers().displayAlert("ERROR!", msg:"4 Digit PIN Required")
        }
        
        else {
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                
                let bet9ja_verify_url = Constants.BASE_URL+"method=betnaijanameenquiry&customerId=\(CustomerID as String)"
                
                getNameEnquiry(url_: bet9ja_verify_url)
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
        }
        
    }
   
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            
            switch (identifier) {
                
            case "pop_bet9ja":
                
                if let vc = segue.destination as? Options_TableViewController {
                    
                    vc.table_items =  ["Agent","Subscriber"]
                    
                    vc.delegate = self
                }
            
                
                
            default:
                break;
            }
            
        }
        
    }
    
    
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        
        return UIModalPresentationStyle.none
    }
    

}
