//
//  Cashout_ViewController.swift
//  PiDO
//
//  Created by sp developer on 11/21/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

class Cashout_ViewController: UIViewController, UITextFieldDelegate {

    var indicator : ActivityIndicator!
    
    var Amount : NSString = ""
    var Paycode : NSString = ""
    var Pin : NSString = ""
    var Charge : NSString = ""
    var db_MobileNo : String = ""
    
    @IBOutlet weak var mAmount: UITextField!
    @IBOutlet weak var mPaycode: UITextField!
    @IBOutlet weak var mPIN: UITextField!
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(Cashout_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
        
        
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            
            
        }
    }
    
    
    // fuction to display alert to the UI
    func displayAlert(_ mTitle : String, msg : String) -> Void{
        let alertView : UIAlertView = UIAlertView()
        alertView.title = mTitle
        alertView.message = msg
        alertView.delegate = self
        alertView.addButton(withTitle: "OK")
        alertView.show()
    }
    
    // retrieve transaction charge from service
    func getCharge(url_: String){
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                
                if let error = error {
                    
                    print("\(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    self.indicator.stopAnimating()
                    
                    if (responseStr.contains("Not available") == false){
                        
                        let charge = responseStr
                        
                        self.Cashout_confirmation_Alert(charge, paycode: self.Paycode, amount: self.Amount)
                        
                    }
                        
                    else {
                        
                        let charge = "Unavailable"
                        
                        self.Cashout_confirmation_Alert(charge as NSString, paycode: self.Paycode, amount: self.Amount)
                        
                    }
                    
                }
                
            }
            
        }
        task.resume()
        
    }
    
    
    func Cashout_confirmation_Alert(_ charge : NSString, paycode : NSString, amount : NSString){
        
        let total = Int((charge as String))!  + Int((amount as String))!
        
        let msg = "Paycode : \(paycode)\n\nAmount : ₦\(amount)\n\nTransaction Charge : ₦\(charge)\n\nTotal : ₦\(total)"
        
        let controller2 = UIAlertController(title: "Transaction Details", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Dismiss" , style: UIAlertActionStyle.cancel, handler: nil)
        let ActionButton = UIAlertAction(title: "Proceed",style: .destructive, handler: { action in
            
             let encodedPaycode = UtilHelpers().toBase64(self.Paycode as String)
             let encodedMobile = UtilHelpers().toBase64(self.db_MobileNo)
             let encodedPIN = UtilHelpers().toBase64(self.Pin as String)
            
            let cashout_url = Constants.BASE_URL+"method=cashout&amount=\(self.Amount as String)&mobile=\(encodedMobile)&paycode=\(encodedPaycode)&pin=\(encodedPIN)&charge=\(charge as String)"
            
            self.Cashout_Task(url_: cashout_url)
            
            
        })
        
        controller2.addAction(cancelAction)
        controller2.addAction(ActionButton)
        self.present(controller2, animated: true, completion: nil)
        
        
        
    }
    
    
    func Cashout_Task(url_: String){
        
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                
                if let error = error {
                    
                    print("\(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                    self.mAmount.text! = ""
                    self.mPaycode.text! = ""
                    self.mPIN.text! = ""
                    
                }
                
                
                
            }
            
            
        }
        task.resume()
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == mPaycode || textField == mPIN){
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
        
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }

   
    @IBAction func CashOut_Btn(_ sender: UIButton){
        
        Amount = mAmount.text! as NSString
        Paycode = mPaycode.text! as NSString
        Pin = mPIN.text! as NSString
        mAmount.resignFirstResponder()
        mPaycode.resignFirstResponder()
        mPIN.resignFirstResponder()
        
        if(mAmount.text!.isEmpty && mPaycode.text!.isEmpty && mPIN.text!.isEmpty){
            
            displayAlert("ERROR!",msg: "Insert the required details into text fields")
        }
        
        else if(Amount.length == 0){
             displayAlert("ERROR!",msg: "Amount text field is required")
        }
        
        else if(Paycode.length == 0){
            
             displayAlert("ERROR!",msg: "4 Digit Paycode Required")
        }
        
        else if(Paycode.length < 4){
            
            displayAlert("ERROR!",msg: "4 Digit Paycode Required")
            
        }
        
        else if(Pin.length == 0){
            
            displayAlert("ERROR!",msg: "4 Digit PIN Required")
            
        }
            
        else if(Pin.length < 4){
            
            displayAlert("ERROR!",msg: "4 Digit PIN Required")
            
        }
        
        else{
            
            if Reachability.isConnectedToNetwork() {
                
                let cashout_charge_url = Constants.BASE_URL+"method=cashoutcharge&amount=\(Amount)&mobile=\(db_MobileNo)"
                
                getCharge(url_: cashout_charge_url)

            }
            
            else {
                
                 UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
            }
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
