//
//  ChangePin_ViewController.swift
//  PiDO
//
//  Created by sp developer on 11/21/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

class ChangePin_ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var mOldPIN: UITextField!
    @IBOutlet weak var mNewPIN: UITextField!
    @IBOutlet weak var mConfirm: UITextField!
    
    var db_MobileNo = ""
    
    var indicator : ActivityIndicator!
    
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(ChangePin_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            
            
        }
        
    }
    
    // fuction to display alert to the UI
    func displayAlert(_ mTitle : String, msg : String) -> Void{
        let alertView : UIAlertView = UIAlertView()
        alertView.title = mTitle
        alertView.message = msg
        alertView.delegate = self
        alertView.addButton(withTitle: "OK")
        alertView.show()
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 4
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
        currentString.replacingCharacters(in: range, with: string) as NSString
        
        return newString.length <= maxLength

        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }
    
    func ChangePIN_Task(url_: String) -> Void{
        
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        //   UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        
        var responseStr: NSString = ""
        
        let urlString = url_
        let url = URL(string: urlString)
        //var request = NSURLRequest(URL: url!)
        let request = NSMutableURLRequest(url: url!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                
                if(data!.count < 0){
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else{
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                    self.mOldPIN.text = ""
                    
                    self.mNewPIN.text = ""
                    
                    self.mConfirm.text = ""
                    
                    
                }
                
            }
            
           
            
        }
        
        task.resume()

        
    }
    
    

    
    @IBAction func ChangePin_Btn(_ sender: UIButton) {
        
        let OldPIN = mOldPIN.text! as NSString
        let NewPIN = mNewPIN.text! as NSString
        let Confirm = mConfirm.text! as NSString
        
        if(mOldPIN.text!.isEmpty && mNewPIN.text!.isEmpty && mConfirm.text!.isEmpty ){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Insert the required details into text fields")
        }
        
        else if(OldPIN.isEqual(NewPIN)){
            
            UtilHelpers().displayAlert("ERROR!", msg: "New PIN Must Be Different From Old PIN")
        }
        
        else if(NewPIN.isEqual(Confirm) == false){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Passwords Do Not Match")
        }
        
        else if(OldPIN.length < 4 &&  NewPIN.length < 4 && Confirm.length < 4){
            
            UtilHelpers().displayAlert("ERROR!", msg: " 4 Digit Password Required")
            
        }
        
        else if(NewPIN.length < 4){
            
            UtilHelpers().displayAlert("ERROR!", msg: " 4 Digit New PIN Required")
            
        }
            
        
        else {
            
            mOldPIN.resignFirstResponder()
            mNewPIN.resignFirstResponder()
            mConfirm.resignFirstResponder()
                    
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                
                let encodedMobileno = UtilHelpers().toBase64(self.db_MobileNo)
                let encodedOldPIN = UtilHelpers().toBase64(OldPIN as String)
                let encodedNewPIN = UtilHelpers().toBase64(NewPIN as String)
                let encodedConfirmPIN = UtilHelpers().toBase64(Confirm as String)
                
                let pin_url = Constants.BASE_URL+"method=changepin&mobileno=\(encodedMobileno)&oldpin=\(encodedOldPIN)&newpin=\(encodedNewPIN)&confirmpin=\(encodedConfirmPIN)"

                self.ChangePIN_Task(url_: pin_url)
                
               
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
            
        }
        
    }

    
}
