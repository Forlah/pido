//
//  Constants.swift
//  PiDO
//
//  Created by sp developer on 5/21/16.
//  Copyright © 2016 sp developer. All rights reserved.
//

import Foundation

class Constants {

    static let TIMEOUT_INTERVAL = 12.0
    

    //static let BASE_URL = "http://62.173.41.6:8081/paycommobilemoney_new/platform/pido2.ashx?"
    static let AppVersionCode = "iOS 1.2.3"
    static let BASE_URL = "http://10.10.10.15:8081/paycommobilemoney_new/platform/pido2.ashx?"
    
}
