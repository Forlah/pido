//
//  Contact.swift
//  PiDO
//
//  Created by sp developer on 1/1/16.
//  Copyright (c) 2016 sp developer. All rights reserved.
//

import Foundation

class Contact {
    
    
    fileprivate var id : Int = -1
    fileprivate var firstname : String = ""
    fileprivate var surname : String = ""
    fileprivate var bank : String = ""
    fileprivate var phonenumber: String = ""
    fileprivate var accountnumber : String = ""
    
    
    
    // empty constructor
    init(){
        
    }
    
    
    init(_id : Int , _firstname : String , _surname : String , _bank : String , _phonenumber : String , _accountnumber : String){
        
        id = _id
        firstname = _firstname
        surname = _surname
        bank = _bank
        phonenumber = _phonenumber
        accountnumber = _accountnumber
        
    }
    
    init(_firstname : String , _surname : String , _bank : String , _phonenumber : String , _accountnumber : String){
        
        firstname = _firstname
        surname = _surname
        bank = _bank
        phonenumber = _phonenumber
        accountnumber = _accountnumber
        
    }
    
    // constructor with with fullname and surname together
    init(_names : String , _bank : String , _phonenumber : String , _accountnumber : String){
        
        firstname = _names
        bank = _bank
        phonenumber = _phonenumber
        accountnumber = _accountnumber
        
    }
    
    func getid() -> Int {
        
        return id
    }
    
    func setid(_ _id : Int) {
        id = _id
    }
    
    func getFirstname() -> String {
        return firstname
    }
    
    
    func setFirstname(_ _firstname : String ){
        firstname = _firstname
    }
    
    
    func getSurname() -> String {
        return surname
    }
    
    
    func setSurname(_ _surname : String ){
        surname = _surname
    }
    
    
    func getBank() -> String {
        return bank
    }
    
    
    func setBank(_ _bank : String ){
        bank = _bank
    }
    
    
    func getPhoneNumber() -> String {
        return phonenumber
    }
    
    
    func setPhoneNumber(_ _phonenumber : String ){
        phonenumber = _phonenumber
    }
    
    
    func getAccountNumber() -> String {
        return accountnumber
    }
    
    
    func setAccountNumber(_ _accountnumber : String ){
        accountnumber = _accountnumber
    }
    
    
    
}
