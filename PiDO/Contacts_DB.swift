//
//  Contacts_DB.swift
//  PiDO
//
//  Created by sp developer on 1/1/16.
//  Copyright (c) 2016 sp developer. All rights reserved.
//

import Foundation

class Contacts_DB {
    
    let DATABASE_NAME : String = "ContactsManager"
    let DATABASE_TABLE_NAME = "Pidocontacts"
    
    // contacts table column names
    let KEY_ID = "id";
    let KEY_SURNAME = "SURNAME";
    let KEY_FIRSTNAME = "FIRST_NAME";
    let KEY_PHONE_NO = "PHONE_NUMBER";
    let KEY_BANK = "BANK";
    let KEY_ACCOUNT_NO = "ACCOUNT_NUMBER";
    
    
    func getDatabasePath() -> String {
        
        let Filemgr = FileManager.default
        let dirPaths = Filemgr.urls(for: .documentDirectory, in: .userDomainMask)
        
        let databasePath = dirPaths[0].appendingPathComponent("\(DATABASE_NAME).db").path
        
        return databasePath
    }
    
    
    func isExisting() -> Bool {
        
        if getAll().count < 1{
            return false

        }
            
        
        else {
            return true
        }
            
        

    }
    
    
    
    func createTable() {
        
       let Filemgr = FileManager.default
       let databasePath = getDatabasePath()
      
        
        if Filemgr.fileExists(atPath: databasePath as String) { // checks if file path exists
            
            let database = FMDatabase(path: databasePath as String)
            
            if database == nil {
                print("Error opening database \(DATABASE_NAME)   \(String(describing: database?.lastErrorMessage()))")
            }
            
            
            if(database?.open())! {
                
                let sql_create_stmnt = "CREATE TABLE IF NOT EXISTS \(DATABASE_TABLE_NAME) (\(KEY_ID) INTEGER PRIMARY KEY AUTOINCREMENT, \(KEY_SURNAME) TEXT, \(KEY_FIRSTNAME) TEXT, \(KEY_PHONE_NO) TEXT, \(KEY_BANK) TEXT, \(KEY_ACCOUNT_NO) TEXT);"
                
                if !(database?.executeStatements(sql_create_stmnt))! {
                    print("ERROR creating database  \(String(describing: database?.lastErrorMessage()))")
                }
                
                database?.close()
                print("Database created !")
                
            } else {
                print("ERROR could not open database \(String(describing: database?.lastErrorMessage()))")
            }
            
        }
        
        else {
            print("File not existing , database not created")
        }
    }
    
    
    // add a contact to database table
    func saveContact(_ contact : Contact){
        
        let database = FMDatabase(path: getDatabasePath() as String)
        
        if (database?.open())! {
            
            let insert_sql = "INSERT INTO \(DATABASE_TABLE_NAME) (\(KEY_SURNAME), \(KEY_FIRSTNAME), \(KEY_PHONE_NO), \(KEY_BANK), \(KEY_ACCOUNT_NO)) VALUES ('\(contact.getSurname())', '\(contact.getFirstname())', '\(contact.getPhoneNumber())', '\(contact.getBank())', '\(contact.getAccountNumber())');"
            
           let result =  database?.executeUpdate(insert_sql, withArgumentsIn: nil)
            
            if !result! {
                
                print("Error adding contact  \(String(describing: database?.lastErrorMessage()))")
                
            }
            
            else {
                print("Contact Added successfully")
            }
            
        }
        
        else {
            print("Error : \(String(describing: database?.lastErrorMessage()))")
        }
        
    }
    
    
    
    func getAll() -> [Contact] {
        
        var contactList : [Contact]
        contactList = [Contact]() // intialization
        
        
        let database = FMDatabase(path: getDatabasePath() as String)
        
        if (database?.open())! {
            
            let select_query = "SELECT * FROM \(DATABASE_TABLE_NAME)"
            let results : FMResultSet? = database?.executeQuery(select_query, withArgumentsIn: nil)
            

            while (results?.next() == true ){
                
                let id = results?.long(forColumn: "ID")
                let surname = results?.string(forColumn: "SURNAME")
                let firstname =  results?.string(forColumn: "FIRST_NAME")
                let phone = results?.string(forColumn: "PHONE_NUMBER")
                let bank = results?.string(forColumn: "BANK")
                let accNo = results?.string(forColumn: "ACCOUNT_NUMBER")
                
                
                
                let _contact = Contact()
                _contact.setid(id!)
                _contact.setSurname(surname!)
                _contact.setFirstname(firstname!)
                _contact.setPhoneNumber(phone!)
                _contact.setBank(bank!)
                _contact.setAccountNumber(accNo!)
                
                contactList.append(_contact)
            }
            
            database?.close()
            
        }
        
        else {
            print("Error: \(String(describing: database?.lastErrorMessage()))")
        }
            
            
        
        
        return contactList
        
    }
    
    
    func updateContact(_ contact : Contact, id : Int) {
        
        
        let database = FMDatabase(path: getDatabasePath() as String)
        if (database?.open())! {
            
            let updateQuery = "UPDATE \(DATABASE_TABLE_NAME) SET \(KEY_SURNAME) = '\(contact.getSurname())', \(KEY_FIRSTNAME) = '\(contact.getFirstname())', \(KEY_PHONE_NO) = '\(contact.getPhoneNumber())', \(KEY_BANK) = '\(contact.getBank())', \(KEY_ACCOUNT_NO) = '\(contact.getAccountNumber())' WHERE \(KEY_ID) = '\(id)' "
            
            let updateSuccessful =  database?.executeUpdate(updateQuery, withArgumentsIn: nil)
            
            if(!updateSuccessful!){
                print("UPDATE FAILED: \(String(describing: database?.lastErrorMessage()))")
            }
                
            else{
                print("UPdate Successful!")
            }

            
        }
        
        
        
    }
        
        
        
        
        
        
        
    
    
}
