//
//  Contacts_TableViewController.swift
//  PiDO
//
//  Created by sp developer on 1/4/16.
//  Copyright (c) 2016 sp developer. All rights reserved.
//

import UIKit

class Contacts_TableViewController: UITableViewController {
    
    var db_Contacts : [Contact] = []
    var length : Int = 1
    var rowContact =  Contact()
    var items : [String] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()

        db_Contacts = Contacts_DB().getAll()
        length = db_Contacts.count
     
    }
    
    func Contact_Updater(_ value: [Contact]) {
        db_Contacts = value
      //  self.tableView.reloadData()
        tableView.insertRows(at: [IndexPath(row: (db_Contacts.count - 1), section: 0)], with:.none )
    }
    
    
    @IBAction func unwindToContacts(segue: UIStoryboardSegue) { // unwind segue after adding new contacts to show new contacts that have been added
        
        db_Contacts = Contacts_DB().getAll()
        length = db_Contacts.count
        
        tableView.reloadData() // reload tableview datasource
        
        
    }
    
 

    @IBAction func AddContacts_Btn(_ sender: UIBarButtonItem) {
        
        performSegue(withIdentifier: "newcontact", sender: self)
        
    }
  
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return length
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "contacts_cell", for: indexPath) 
        
        
        let item = db_Contacts[indexPath.row]
        
        let fullname_txt = "\(item.getSurname()) \(item.getFirstname())"
        let bank = item.getBank()
        
        cell.textLabel?.text = fullname_txt
        cell.detailTextLabel?.text = bank
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        
        //UITableViewCell cell = self.tableView cellForRowAtIndexPath:indexPath
        let cell = self.tableView .cellForRow(at: indexPath)
        
        
        
         let controller = UIAlertController(title: "Select Action", message:nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let airtimeAction = UIAlertAction(title: "Perform Airtime Topup", style: UIAlertActionStyle.default) { (airtimeAction) -> Void in
            
            self.performSegue(withIdentifier: "contacts_airtime", sender: self)
            
        }
        
        let pidoAction = UIAlertAction(title: "Transfer To PiDO", style: UIAlertActionStyle.default) { (pidoAction) -> Void in
            
           self.performSegue(withIdentifier: "contacts_pido", sender: self)
            
        }
        
        let bankAction = UIAlertAction(title: "Transfer To Bank", style: UIAlertActionStyle.default) { (bankAction) -> Void in
            
            self.performSegue(withIdentifier: "contacts_bank", sender: self)
            
        }
        
        let EditAction = UIAlertAction(title: "Edit Contact", style: UIAlertActionStyle.default) { (EditAction) -> Void in
            
            self.performSegue(withIdentifier: "contacts_editcontact", sender: self)
            
        }
        
        rowContact = db_Contacts[indexPath.row]
        
       // var id = rowContact.getid()
        let firstname = rowContact.getFirstname()
        let surname = rowContact.getSurname()
        let phone = rowContact.getPhoneNumber()
        let accNo = rowContact.getAccountNumber()
        let bank = rowContact.getBank()
        
        items.append(firstname)
        items.append(surname)
        items.append(phone)
        items.append(accNo)
        items.append(bank)
        
        
        
        
        
        controller.addAction(airtimeAction)
        controller.addAction(pidoAction)
        controller.addAction(bankAction)
        controller.addAction(EditAction)
        
        if let ppc = controller.popoverPresentationController{
            ppc.sourceView =  cell
            ppc.sourceRect =  (cell?.bounds)!
        }
        


        
        present(controller, animated: true, completion: nil)
        
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            
            var firstname = ""
            var surname = ""
            var phone = ""
            var acc = ""
            var bank = ""
            
            if items.count > 0 {
                
                 firstname = items[0]
                 surname = items[1]
                 phone = items[2]
                 acc = items[3]
                 bank = items[4]
                
            }
            
            switch identifier {
                
                case "contacts_editcontact":
                    
                    if let vc = segue.destination as? AddContact_ViewController {
                        
                      let id = rowContact.getid()
                        
//                        vc.ID = id
//                        vc.Firstname = items[0]
//                        vc.Surname = items[1]
//                        vc.PhoneNumber = items[2]
//                        vc.AccNumber = items[3]
//                        vc.Bank = items[4]
                        
                        vc.ID = id
                        vc.Firstname = firstname
                        vc.Surname = surname
                        vc.PhoneNumber = phone
                        vc.AccNumber = acc
                        vc.Bank = bank
                        
                        items.removeAll(keepingCapacity: true) // clear the array list of items
                        
                        
                    }
                
             case "contacts_airtime":
                
                if let vc = segue.destination as? AirtimeTopup_ViewController{
                    
                    vc.Recipient = phone as NSString
                    
                    items.removeAll(keepingCapacity: true) // clear the array list of items
                    
                    
                }
                
                
            case "contacts_pido":
                
                if let vc = segue.destination as? PidoTransfer_ViewController {
                    
                    vc.Recipient = phone as NSString
                    
                    items.removeAll(keepingCapacity: true) // clear the array list of items
                    
                    
                }
                
                
            case "contacts_bank":
                
                if let vc = segue.destination as? BankTransfer_ViewController {
                    
                    vc.Recipient = phone as NSString
                    vc.bank_name = bank
                    vc.AccNumber = acc as NSString
                    
                    items.removeAll(keepingCapacity: true) // clear the array list of items
                    
                    
                }
                
                
                
                
                
                
               default:
                  break
            }
            
            
            
        }
        
        
        
    }
    

}
