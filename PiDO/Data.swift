//
//  Data.swift
//  PiDO
//
//  Created by sp developer on 1/7/16.
//  Copyright (c) 2016 sp developer. All rights reserved.
//

import Foundation

class Data {
    
    fileprivate var firstname : String = ""
    fileprivate var surname : String = ""
    fileprivate var gender : String = ""
    fileprivate var PiDOMobile: String = ""
    fileprivate var Email : String = ""
    fileprivate var Password : String = ""
    fileprivate var AuthCode : String = ""
    fileprivate var token : String = ""
    
    
     init() {
        
    }
    
    func setFirstname(_ _firstname : String){
        
        firstname = _firstname
    }
    
    func getFirstname() -> String{
        
        return firstname
    }
    
    func setSurname(_ _surname : String){
        
        surname = _surname
    }
    
    func getSurname() -> String {
        
        return surname
    }
    
    func setGender(_ _gender : String) {
        gender = _gender
    }
    
    func getGender() -> String {
        
        return gender
    }
    
    func setPidoMobile(_ _mobileno : String){
        
        PiDOMobile = _mobileno
    }
    
    func getPidoMobile() -> String {
        
        return PiDOMobile
    }
    
    func setEmail(_ email : String){
        
        Email = email
    }
    
    func getEmail() -> String {
        return Email
    }
    
    func getPassword() -> String {
        
        return Password
    }
    
    func setPassword(_ _password: String){
        
        Password = _password
    }
    
    func setAuthcode(_ _authcode : String){
        
        AuthCode = _authcode
    }
    
    func getAuthCode() -> String{
        
        return AuthCode
    }

    
    func setToken(_ _token : String){
        
        token = _token
    }
    
    func getToken() -> String{
        
        return token
    }

    
    
    
    
    
    
}
