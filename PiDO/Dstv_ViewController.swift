//
//  Dstv_ViewController.swift
//  PiDO
//
//  Created by sp developer on 12/11/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

class Dstv_ViewController: UIViewController, UIPopoverPresentationControllerDelegate,Selected_Option, UITextFieldDelegate {

    @IBOutlet weak var bouquet_label: UITextField!
    @IBOutlet weak var mPIN: UITextField!
    @IBOutlet weak var mCustomerID: UITextField!
    
    var indicator : ActivityIndicator!
    
    var CustomerID : NSString = ""
    var PIN : NSString = ""
    var bouquet : NSString = ""
    
    var db_MobileNo = ""
    
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
    func fromTableview(_ value: String) {
        
        bouquet_label.text = value
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            
            
        }
        
        bouquet_label.text = "DSTV COMPACT PLUS"
        
        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(Dstv_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == bouquet_label {
            return false
        }
            
        else {
            return true
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == mPIN){
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
            
        else {
            return true
        }
        
    }
    
    func DSTV_task(url_: String){
        
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        var responseStr: NSString = ""
        let safeURL = url_.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let request = NSMutableURLRequest(url: URL(string: safeURL)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    print("\(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                    self.mCustomerID.text! = ""
                    self.mPIN.text! = ""
                    
                }
                
            }
            
            
        }
        
        task.resume()
        
    }
    
    func dstv_verify_task(url_: String){
        
        var amount : NSString = ""
        var trans_charge : NSString = ""
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let safeURL = url_.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let request = NSMutableURLRequest(url: URL(string: safeURL!)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    print("\(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
                    
                    self.indicator.stopAnimating()
                    
                    print("Response from service : \(responseStr)")
                    
                    if(responseStr.hasPrefix("|0||400")) {
                        
                        UtilHelpers().displayAlert("FAILED", msg: "Please Check Your Customer ID")
                        
                    }
                        
                    else if(responseStr.contains("|")){
                        
                        
                        let seperated_String = responseStr.components(separatedBy: "|")
                        
                        trans_charge = seperated_String[0] as NSString
                        amount = seperated_String[1] as NSString
                        
                        
                        self.indicator.stopAnimating() // dismiss indicator
                        
                        
                        // display a dialog for the user to verify the acc
                        let total = (trans_charge as NSString).integerValue + (amount as NSString).integerValue
                        
                        let msg = "Customer ID : \(self.CustomerID)\n\nBouquet : \(self.bouquet)\n\nAmount : ₦\(amount)\n\nTransaction Charge : ₦\(trans_charge)\n\nTotal : ₦\(total)"
                        
                        let controller = UIAlertController(title: "Transaction Details", message: msg, preferredStyle: UIAlertControllerStyle.alert)
                        let cancelAction = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil)
                        let ActionButton = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.destructive, handler: { action in
                            
                            let encodedeCustomerId = UtilHelpers().toBase64(self.CustomerID as String)
                            let encodedMobileNo = UtilHelpers().toBase64(self.db_MobileNo)
                            let encodedPIN = UtilHelpers().toBase64(self.PIN as String)
                            
                            let dstv_url = Constants.BASE_URL+"method=billspayment&customerid=\(encodedeCustomerId)&mobile=\(encodedMobileNo)&pin=\(encodedPIN)&product=\(self.bouquet as String)"
                            
                            
                            
                            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                                
                                self.DSTV_task(url_: dstv_url)
                            }
                                
                            else {
                                
                                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                                
                            }
                            
                            
                            
                        })
                        
                        
                        controller.addAction(cancelAction)
                        controller.addAction(ActionButton)
                        self.present(controller, animated: true, completion: nil)
                        
                        
                    }
                        
                        
                    else {
                        
                        
                        UtilHelpers().displayAlert("Response", msg: responseStr as String)
                        
                    }
                    
                }
                
            }
            
        }
        
        task.resume()
        
    }
    
    
    @IBAction func Bouquet_Btn(_ sender: UIButton) {
        
        performSegue(withIdentifier: "pop_dstv", sender: self)

    }
    
    @IBAction func Dstv_Btn(_ sender: UIButton) {
        
        mCustomerID.resignFirstResponder()
        mPIN.resignFirstResponder()
        
        CustomerID = mCustomerID.text! as NSString
        PIN = mPIN.text! as NSString
        bouquet = bouquet_label.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        
        
        if(mCustomerID.text!.isEmpty && mPIN.text!.isEmpty){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Insert the required details into text fields")
            
        }
            
        else if(CustomerID.length == 0) {
            
            UtilHelpers().displayAlert("ERROR!", msg: "DSTV Customer ID Is Required")
        }
            
        else if(PIN.length == 0){
            UtilHelpers().displayAlert("ERROR!", msg: "4 Digit PIN Required")
            
        }
            
        else if(PIN.length < 4){
            
            UtilHelpers().displayAlert("ERROR!", msg: "4 Digit PIN Required")
            
        }
            
        else {
            
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                let http = Constants.BASE_URL
                let main = "method=billscharges&productname=\(bouquet)&mobileno=\(db_MobileNo)"
                
                let dstv_acc_url = http + main
                
                dstv_verify_task(url_: dstv_acc_url)
                
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
            
        }
        
        
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            
            switch (identifier) {
                
            case "pop_dstv":
                
                if let vc = segue.destination as? Options_TableViewController {
                    
                    vc.table_items = ["DSTV COMPACT PLUS","DSTV FAMILY","DSTV PREMIUM","DSTV ACCESS","DSTV COMPACT","DSTV PREMIUM EXTRA","DSTV FTA PLUS"]
                    
                    vc.delegate = self
                }
                
            default:
                break;
            }
            
        }
        
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        
        return UIModalPresentationStyle.none
    }
    

}
