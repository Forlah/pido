//
//  ForgotPassword_ViewController.swift
//  PiDO
//
//  Created by sp developer on 11/26/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

class ForgotPassword_ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var mSQuestion: UITextField!
    @IBOutlet weak var mSAnswer: UITextField!
    @IBOutlet weak var mNewPassword: UITextField!
    @IBOutlet weak var mConfirm: UITextField!
    
    var indicator : ActivityIndicator!
    
    var SAnswer : NSString = ""
    var NewPassword : NSString = ""
    var Confirm : NSString = ""
    var SQuestion : NSString = ""
    
    var db_MobileNo = ""
    var db_UniqueId = ""
    
    var db_ID = -1
    
    let AppVersion = "iOS1.0"
    
    
    
    
    // dismiss keyboard
    func didTapView(){
        
      self.view.endEditing(true)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(ForgotPassword_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            db_UniqueId = db_Reg[0].getUniqueID()
        
            db_ID = db_Reg[0].getid()
            
            
        }
        
        if Reachability.isConnectedToNetwork() {
            
            // fetch user's security question on startup
           getSecurityQuestion_task()
            
        }
            
        else {
            
            UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
        }

        
        

    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    func getSecurityQuestion_task(){
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        
        let urlString = Constants.BASE_URL+"method=test&fone=\(db_MobileNo)"
        let request = NSMutableURLRequest(url: URL(string: urlString)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    print( error)
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    self.indicator.stopAnimating()
                    
                    if (responseStr.contains("|") ){
                        
                        let seperated_String = responseStr.components(separatedBy: "|")
                        let sQuestion = seperated_String[0]
                        self.mSQuestion.text = sQuestion
                    }
                        
                    else {
                        
                        
                        UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    }
                    
                }
                
            }
            
        }
        
        
        task.resume()
        
    }
    
    func updateDb_Task(url_: String){
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    print("failure \(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    if (responseStr.isEqual(to: "1") ){
                        
                        let _user = User()
                        _user.setid(self.db_ID)
                        _user.setPassword(self.NewPassword as String)
                        
                        if (PiDOUser_DB().updatePiDOUser(_user, id: self.db_ID)  == 1 ){
                            
                            self.indicator.stopAnimating()
                            
                            UtilHelpers().displayAlert("Success", msg: "Password Change Successful")
                            
                            
                            if let navController = self.navigationController {
                                navController.popViewController(animated: true)
                            }
                            
                        }
                    }
                        
                        
                    else {
                        
                        self.indicator.stopAnimating()
                        
                        UtilHelpers().displayAlert("RESPONSE", msg: "Error Occured , Try again")
                    }
                    
                }
                
            }
            
            
        }
        
        task.resume()
    }
    
    
    func SecurityQuestion_task(url_: String){
        
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    print("failure \(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                }
                    
                else  {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    if (responseStr.contains("8001") ){
                        
                        let update_uri = Constants.BASE_URL+"method=forgetpassword&mobile=\(self.db_MobileNo)&autcode=\(self.db_UniqueId)&versiself.on=\(self.AppVersion)&password=\(self.NewPassword as String)&securityanswer=\(self.SAnswer as String)"
                        
                        self.updateDb_Task(url_: update_uri)
                        
                    }
                        
                    else if(responseStr.contains("8002")){
                        self.indicator.stopAnimating()
                        
                        UtilHelpers().displayAlert("RESPONSE", msg: "Invalid Credentials")
                        
                    }
                        
                    else {
                        
                        self.indicator.stopAnimating()
                        
                        UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    }
                    
                }
                
            }
            
        
        }
        
        task.resume()
        
    }
    
    

    @IBAction func changePasswrd_Btn(_ sender: AnyObject) {
        
        mSQuestion.resignFirstResponder()
        mSAnswer.resignFirstResponder()
        mNewPassword.resignFirstResponder()
        mConfirm.resignFirstResponder()
        
        SQuestion = mSQuestion.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString

        SAnswer = mSAnswer.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        NewPassword = mNewPassword.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        
        Confirm = mConfirm.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        
        if(SQuestion.length == 0 && SAnswer.length == 0 && NewPassword.length == 0 &&  Confirm.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Insert the required details into text fields")
            
        }
            
        else if(SAnswer.length == 0 && NewPassword.length == 0 &&  Confirm.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Insert the required details into text fields")
            
        }
        
        else if(SAnswer.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Security Answer Is Required")
            
        }
        
        else if(NewPassword.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "New Password Is Required")
            
        }
        
        else if(NewPassword.length < 6){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Password Must Be Six Characters Or More")
            
        }
        
        else if(Confirm.isEqual(to: NewPassword as String) == false){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Passwords Do Not Match")
            
        }
        
        
        else {
            
            if Reachability.isConnectedToNetwork() {
                
                let security_question_url = Constants.BASE_URL+"method=checksecurityquestion&msisdn=\(db_MobileNo)&autcode=\(db_UniqueId)&securityanswer=\(SAnswer)&version=\(AppVersion)"
                
                SecurityQuestion_task(url_: security_question_url)

            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
            }
            
            
        }


        
        
//        performSegueWithIdentifier("ForgotPasswordToLogin", sender: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
