//
//  GOtv_ViewController.swift
//  PiDO
//
//  Created by sp developer on 12/9/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

class GOtv_ViewController: UIViewController, UIPopoverPresentationControllerDelegate ,UITextFieldDelegate, Selected_Option {
    
    @IBOutlet weak var bouquet_label: UITextField!
    
    @IBOutlet weak var mPIN: UITextField!
    @IBOutlet weak var mCustomerId: UITextField!
    var db_MobileNo = ""
    
    var indicator : ActivityIndicator!
    
    var CustomerID : NSString = ""
    var PIN : NSString = ""
    var bouquet : NSString = ""
    
    
    func pop(){
        performSegue(withIdentifier: "pop_gotv", sender: self)
        
    }
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
    func fromTableview(_ value: String) {
        
        bouquet_label.text = value
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            
            
        }
        
        // Do any additional setup after loading the view.
        
        bouquet_label.text = "GOTV"
        
        
        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(GOtv_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
        
        
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == bouquet_label {
            return false
        }
            
        else {
            return true
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == mPIN){
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
            
        else {
            return true
        }
        
    }
    
    
    
    func fromGotvPopover(_ value: String) {
        
        bouquet_label.text = value
    }
    
    func GOTV_task(url_: String){
        
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    print( (error))
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                    self.mCustomerId.text! = ""
                    self.mPIN.text! = ""
                }
                
            }
         
            
        }
        
        task.resume()
        
    }
    
    func gotv_verify_task(url_: String){
        
        var amount : NSString = ""
        var trans_charge : NSString = ""
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){data, response, error in
            DispatchQueue.main.async {
                
                
                if let error = error {
                    print( (error))
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    self.indicator.stopAnimating()
                    print("Response from service : \(responseStr)")
                    
                    if(responseStr.hasPrefix("|0||400")) {
                        
                        UtilHelpers().displayAlert("FAILED", msg: "Please Check Your Customer ID")
                        
                    }
                        
                    else if(responseStr.contains("|")){
                        
                        
                        let seperated_String = responseStr.components(separatedBy: "|")
                        
                        trans_charge = seperated_String[0] as NSString
                        amount = seperated_String[1] as NSString
                        
                        
                        self.indicator.stopAnimating() // dismiss indicator
                        
                        
                        // display a dialog for the user to verify the acc
                        let total = (trans_charge as NSString).integerValue + (amount as NSString).integerValue
                        
                        let msg = "Customer ID : \(self.CustomerID)\n\nBouquet : \(self.bouquet)\n\nAmount : ₦\(amount)\n\nTransaction Charge : ₦\(trans_charge)\n\nTotal : ₦\(total)"
                        
                        let controller = UIAlertController(title: "Transaction Details", message: msg, preferredStyle: UIAlertControllerStyle.alert)
                        let cancelAction = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil)
                        let ActionButton = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.destructive, handler: { action in
                            
                            let encodedeCustomerId = UtilHelpers().toBase64(self.CustomerID as String)
                            let encodedMobileNo = UtilHelpers().toBase64(self.db_MobileNo)
                            let encodedPIN = UtilHelpers().toBase64(self.PIN as String)
                            
                            let gotv_url = Constants.BASE_URL+"method=billspayment&customerid=\(encodedeCustomerId)&mobile=\(encodedMobileNo)&pin=\(encodedPIN)&product=\(self.bouquet as String)"
                            
                            
                            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                                
                                self.GOTV_task(url_: gotv_url)
                            }
                                
                            else {
                                
                                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                                
                            }
                            
                            
                        })
                        
                        controller.addAction(cancelAction)
                        controller.addAction(ActionButton)
                        self.present(controller, animated: true, completion: nil)
                        
                        
                    }
                        
                        
                    else {
                        
                        
                        UtilHelpers().displayAlert("Response", msg: responseStr as String)
                        
                    }
                    
                }
                
            }
            
        }
        
        task.resume()
        
    }
    
    
    
    @IBAction func Options_Btn(_ sender: UIButton) {
         performSegue(withIdentifier: "pop_gotv", sender: self)
    }
    
    @IBAction func GOTV_Btn(_ sender: UIButton) {
        
        mCustomerId.resignFirstResponder()
        mPIN.resignFirstResponder()
        
        CustomerID = mCustomerId.text! as NSString
        PIN = mPIN.text! as NSString
        bouquet = bouquet_label.text! as NSString
        
        
        
        if(mCustomerId.text!.isEmpty && mPIN.text!.isEmpty){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Insert the required details into text fields")
            
        }
            
        else if(CustomerID.length == 0) {
            
            UtilHelpers().displayAlert("ERROR!", msg: "DSTV Customer ID Is Required")
        }
            
        else if(PIN.length == 0){
            UtilHelpers().displayAlert("ERROR!", msg: "4 Digit PIN Required")
            
        }
            
        else if(PIN.length < 4){
            
            UtilHelpers().displayAlert("ERROR!", msg: "4 Digit PIN Required")
            
        }
            
        else {
            
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                let gotv_acc_url = Constants.BASE_URL+"method=billscharges&productname=\(bouquet as String)&mobileno=\(db_MobileNo)"
                
                    gotv_verify_task(url_: gotv_acc_url)
                
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
            
        }
        
        
        
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            
            switch identifier {
                
            case "pop_gotv":
                if let vc = segue.destination as? Options_TableViewController {
                    
                    vc.table_items = ["GOTV","GOTV MOBILE","GOTV PLUS","GOTV LITE ANNUAL"]

                    vc.delegate = self
                }
                
                
            default:
                break
                
            }
        }
        
    }
    
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        
        return UIModalPresentationStyle.none
    }

    

}
