//
//  GetPidoContacts_TableTableViewController.swift
//  PiDO
//
//  Created by sp developer on 4/12/16.
//  Copyright © 2016 sp developer. All rights reserved.
//

import UIKit

@objc protocol Selected_Contact {
    func From_GetPidoContactsTable(_ value: String)
}


class GetPidoContacts_TableTableViewController: UITableViewController {

    var db_Contacts : [Contact] = []
    
    weak var delegate : Selected_Contact?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        db_Contacts = Contacts_DB().getAll()
    }

   
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return db_Contacts.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contact_cell", for: indexPath)

        // Configure the cell...
        
        let rowData = db_Contacts[indexPath.row]
        
        let rowLabel = rowData.getSurname()+" "+rowData.getFirstname()

        cell.textLabel?.text = rowLabel
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let row = indexPath.row
        
        let rowData = db_Contacts[row]
        
        delegate?.From_GetPidoContactsTable(rowData.getPhoneNumber())
        
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    



    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
