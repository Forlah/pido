//
//  Gotv_options_ViewController.swift
//  PiDO
//
//  Created by sp developer on 12/11/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

@objc protocol Selected_Bouquet {
    func fromGotvPopover(_ value: String)
}

class Gotv_options_ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var  Gotv_Options = ["GOTV","GOTV MOBILE","GOTV PLUS"]
    
    weak var delegate : Selected_Bouquet?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return Gotv_Options[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return Gotv_Options.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let selected = pickerView.selectedRow(inComponent: 0)
        delegate?.fromGotvPopover(Gotv_Options[selected])
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
