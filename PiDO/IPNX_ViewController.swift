//
//  IPNX_ViewController.swift
//  PiDO
//
//  Created by sp developer on 12/13/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

class IPNX_ViewController: UIViewController, UIPopoverPresentationControllerDelegate, Selected_Option, UITextFieldDelegate {
    
    var indicator : ActivityIndicator!
    
    @IBOutlet weak var mCustomerID: UITextField!
    @IBOutlet weak var mPIN: UITextField!
    
    var ProductId = -1 // initialize product id
    var Amount = 0
    var trans_charge = 100
    
    var CustomerID : NSString = ""
    var PIN : NSString = ""
    var ipnx_option : NSString = ""
    
    var db_MobileNo = ""
    
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
    
    @IBOutlet weak var ipnx_labele: UITextField!
    
    func fromTableview(_ value: String) {
        
        ipnx_labele.text = value
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        ipnx_labele.text = "Family IPNX Plus"
        
        
        //ipnx_labele.addTarget(self, action: Selector("pop"), forControlEvents: UIControlEvents.TouchDown)
        
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            
        }
        
        
        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(IPNX_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == mPIN){
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
            
        else {
            return true
        }
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }
    
    func IPNX_Task(url_: String){
        
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            if let error = error {
                
                print("failure \(error)")
                
                responseStr = "Unable to connect to the service at the moment, Please try again later"
                self.indicator.stopAnimating()
                
                UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)

            }
            
            else {
                
                print("\(data!.count) bytes of data was returned")
                
                responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                
                self.indicator.stopAnimating()
                
                UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                
                
                self.mCustomerID.text! = ""
                self.mPIN.text! = ""
                
            }
            
        }
        task.resume()
        
    }
    
    
    
    func Ipnx_confirmation_Alert(){
        
        let total = trans_charge  + Amount
        
        
        let msg = "Customer ID : \(CustomerID)\n\nPlan : \(ipnx_option)\n\nAmount : ₦\(Amount)\n\nTransaction Charge : ₦\(trans_charge)\n\nTotal : ₦\(total)"
        
        
        let controller2 = UIAlertController(title: "Transaction Details", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Dismiss" , style: UIAlertActionStyle.cancel, handler: nil)
        let ActionButton = UIAlertAction(title: "Proceed",style: .destructive, handler: {action in
            
            let encodedCustomerId = UtilHelpers().toBase64(self.CustomerID as String)
            let encodedPIN = UtilHelpers().toBase64(self.PIN as String )
            let encodedMobile = UtilHelpers().toBase64(self.db_MobileNo)
            
            
            let ipnx_url = Constants.BASE_URL+"method=billspayment&amount=\(self.Amount)&customerid=\(encodedCustomerId)&mobile=\(encodedMobile)&pin=\(encodedPIN)&productid=\(self.ProductId)&charge=\(self.trans_charge)"
            
            
            if Reachability.isConnectedToNetwork(){ // checks for network connection
                
                self.IPNX_Task(url_: ipnx_url)
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
            
        })
        controller2.addAction(cancelAction)
        controller2.addAction(ActionButton)
        self.present(controller2, animated: true, completion: nil)
        
    }
    
    
   
    @IBAction func ipnxOptions_Btn(_ sender: AnyObject) {
        
        performSegue(withIdentifier: "pop_ipnx", sender: self)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == ipnx_labele {
            return false
        }
            
        else {
            return true
        }
    }
    
    
    @IBAction func IPNX_Btn(_ sender: UIButton) {
        
        ipnx_option = ipnx_labele.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        
        if(ipnx_option.isEqual(to: "Family IPNX Plus")){
            
            Amount = 9450;
            ProductId = 40;
        }
            
        else if(ipnx_option.isEqual(to: "Home Office IPNX")){
            
            Amount = 15750;
            ProductId = 41;
            
        }
            
        else if(ipnx_option.isEqual(to: "Professional IPNX")){
            
            Amount = 21000;
            ProductId = 42;
            
        }
            
        else if(ipnx_option.isEqual(to: "Small Business Plus")){
            Amount = 47250;
            ProductId = 44;
        }
            
        else if(ipnx_option.isEqual(to: "Small Business")){
            Amount = 28350;
            ProductId = 43;
        }
            
        else{
            Amount = 6300;
            ProductId = 39;
        }
        
        
        mCustomerID.resignFirstResponder()
        mPIN.resignFirstResponder()
        
        CustomerID = mCustomerID.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        
        PIN  = mPIN.text! as NSString
        
        if(mCustomerID.text!.isEmpty  && mPIN.text!.isEmpty){
            
            UtilHelpers().displayAlert("ERROR", msg: "Insert the required details into text fields")
        }
            
        else if(CustomerID.length == 0) {
            
            UtilHelpers().displayAlert("ERROR", msg: "IPNX Customer ID Is Required")
        }
            
        else if(PIN.length == 0){
            
            UtilHelpers().displayAlert("ERROR", msg: "4 Digit PIN Required")
            
        }
            
            
        else if(PIN.length < 4){
            
            UtilHelpers().displayAlert("ERROR", msg: "4 Digit PIN Required")
            
        }
            
            
        else {
            
            Ipnx_confirmation_Alert()
            
        }
        
        
    }
    

    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            
            switch (identifier) {
                
            case "pop_ipnx":
                if let vc = segue.destination as? Options_TableViewController {
                    
                    vc.table_items =  ["Family IPNX Plus","Home Office IPNX","Professional IPNX","Small Business","Small Business Plus","Family IPNX"]
                    
                    vc.delegate = self
                }

                
            default:
                break;
            }
            
        }
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        
        return UIModalPresentationStyle.none
    }
    
    
}
