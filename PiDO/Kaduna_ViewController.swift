//
//  Kaduna_ViewController.swift
//  PiDO
//
//  Created by sp developer on 1/30/16.
//  Copyright © 2016 sp developer. All rights reserved.
//

import UIKit

class Kaduna_ViewController: UIViewController , UITextFieldDelegate, Selected_Option {
    
    
    var indicator : ActivityIndicator!
    
    @IBOutlet weak var mMeterNO: UITextField!
    @IBOutlet weak var mAmount: UITextField!
    @IBOutlet weak var mPIN: UITextField!
    
    @IBOutlet weak var meter_type_label: UITextField!
    
    let ProductId = "54"
    var MeterNo : NSString = ""
    var PIN : NSString = ""
    var Amount : NSString = ""
    var meter_type : String = ""
    
    var db_MobileNo = ""
    
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
    func fromTableview(_ value: String) {
        meter_type_label.text = value
    }
    
    func toJSON(address: String, amount_in_kobo: String, customer_name: String, payref: String) -> String {
        
        let dict: NSMutableDictionary = NSMutableDictionary()
        var jsonText: String = ""
        
        dict.setValue(meter_type, forKey: "accountType")
        dict.setValue(MeterNo as String, forKey: "accountNumber")
        dict.setValue(amount_in_kobo, forKey: "amount")
        dict.setValue(address, forKey: "location")
        dict.setValue(customer_name, forKey: "payerName")
        dict.setValue(payref, forKey: "payRef")
        dict.setValue("MOBILE", forKey: "channel")
        dict.setValue(db_MobileNo, forKey: "phoneNumber")
        dict.setValue(db_MobileNo, forKey: "subAgentUID")
        dict.setValue(db_MobileNo, forKey: "msisdn")
        dict.setValue(PIN as String, forKey: "pin")
        
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            
            jsonText = NSString(data: jsonData, encoding:String.Encoding.ascii.rawValue)! as String
            
        } catch {
            print(error)
        }
        
        return jsonText
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        meter_type_label.text = "prepaid"
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            
        }
        
        
        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(Kaduna_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == mPIN){
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
            
        else {
            return true
        }
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }
    
    @IBAction func meter_type_Btn(_ sender: UIButton) {
        
        performSegue(withIdentifier: "pop_kaduna", sender: self)
        
    }
    
    
    
    
    func Kaduna_Task(url_: String){
        
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        //   UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        
        var responseStr: NSString = ""
        let safeURL = url_.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let request = NSMutableURLRequest(url: URL(string: safeURL!)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    print( (error))
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                    
                    self.mMeterNO.text! = ""
                    self.mAmount.text! = ""
                    self.mPIN.text! = ""
                }

                
            }
            
        }
        
        task.resume()
        
    }
    
    func Verify_Task(url_: String){
        
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){data, response, error in
            
            DispatchQueue.main.async {
                
                self.indicator.stopAnimating()
                
                if let error = error {
                    print( (error))
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                }
                
                else {
                    
                    let jsonString = NSString(data: data!, encoding:String.Encoding.utf8.rawValue)! as String
                    
                    do {
                        
                        let json_data = jsonString.data(using: .utf8)!
                        
                        
                        if let jsonResult = try? JSONSerialization.jsonObject(with: json_data) as! [String: AnyObject] {
                            
                                let customer_name = jsonResult["name"] as! String
                                let payref = jsonResult["payRef"] as! String
                                let address = jsonResult["address"] as! String
                                let charge = jsonResult["charge"] as! String
                            
                            
                            // display a dialog for the user to verify the acc
                            let total = (charge as NSString).integerValue + (self.Amount as NSString).integerValue
                            
                            let msg = "Customer Name : \(customer_name)\n\nMeter NO : \(self.MeterNo)\n\nAddress : \(address)\n\nMeter Type : \(self.meter_type)\n\nAmount : ₦\(self.Amount)\n\nTransaction Charge : ₦\(charge)\n\nTotal : ₦\(total)"
                            
                            let controller = UIAlertController(title: "Transaction Details", message: msg, preferredStyle: UIAlertControllerStyle.alert)
                            let cancelAction = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil)
                            let ActionButton = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.destructive, handler: { action in
                                
                                
                                let new_kobo_amt = (self.Amount as NSString).integerValue * 100

                                
                                let jsonvalue = self.toJSON(address: address, amount_in_kobo: "\(new_kobo_amt)", customer_name: customer_name, payref: payref)
                                
                                
                                let kaduna_url = Constants.BASE_URL+"method=kadunaelectricitypayment&jsonString=\(jsonvalue)"
                                
                                if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                                    
                                    self.Kaduna_Task(url_: kaduna_url)
                                }
                                    
                                else {
                                    
                                    UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                                    
                                }
                                
                                
                            })
                            
                            controller.addAction(cancelAction)
                            controller.addAction(ActionButton)
                            self.present(controller, animated: true, completion: nil)
                            
                            
                            
                        }
                            
                            
                            
                            
                        else {
                            
                             UtilHelpers().displayAlert("Response", msg: jsonString)
                            
                        }
                        
                    }
                    
                    
                    
                }

                
            }
            
            
        }
        
        task.resume()
        
    }
    
    
    
    @IBAction func Kaduna_Btn(_ sender: UIButton) {
        
        
        mMeterNO.resignFirstResponder()
        mAmount.resignFirstResponder()
        mPIN.resignFirstResponder()
        
        MeterNo = mMeterNO.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        Amount = mAmount.text! as NSString
        meter_type = meter_type_label.text!.trimmingCharacters(in: CharacterSet.whitespaces) as String
        PIN  = mPIN.text! as NSString
        
        if(mMeterNO.text!.isEmpty && mAmount.text!.isEmpty && mPIN.text!.isEmpty){
            
            UtilHelpers().displayAlert("ERROR", msg: "Insert the required details into text fields")
        }
            
        else if(MeterNo.length == 0) {
            
            UtilHelpers().displayAlert("ERROR!", msg: "Meter Number Is Required")
        }
            
        else if(PIN.length == 0){
            UtilHelpers().displayAlert("ERROR!", msg: "4 Digit PIN Required")
            
        }
            
        else if(Amount.length == 0){
            
            UtilHelpers().displayAlert("ERROR", msg: "Amount Is Required")
            
        }
            
        else if(PIN.length < 4){
            
            UtilHelpers().displayAlert("ERROR!", msg: "4 Digit PIN Required")
            
        }
            
            
        else {
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                let phcn_charge_uri = Constants.BASE_URL+"method=kadunaelectricity&meterType=\(meter_type)&accountNo=\(MeterNo as String)"
                
                print("url = \(phcn_charge_uri)")
                
                Verify_Task(url_: phcn_charge_uri)
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
            
        }
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            
            switch (identifier){
                
            case "pop_kaduna":
                
                if let vc = segue.destination as? Options_TableViewController {
                    
                    vc.table_items = ["prepaid","postpaid"]
                    
                    vc.delegate = self
                }
            default :
                break;
            }
        }

        
    }
    
    
}
