//
//  KanoElectricity_ViewController.swift
//  PiDO
//
//  Created by sp developer on 3/23/16.
//  Copyright © 2016 sp developer. All rights reserved.
//

import UIKit

class KanoElectricity_ViewController: UIViewController, UITextFieldDelegate, Selected_Option {

    var indicator : ActivityIndicator!
    
    @IBOutlet weak var mMeterNO: UITextField!
    @IBOutlet weak var mAmount: UITextField!
    @IBOutlet weak var mPIN: UITextField!
    @IBOutlet weak var payment_type_label:UITextField!
    
    let ProductId = "54"
    var MeterNo : NSString = ""
    var PIN : NSString = ""
    var Amount : NSString = ""
    var payment_type : NSString = ""
    
    var db_MobileNo = ""
    
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
    
    func fromTableview(_ value: String) {
        
        payment_type_label.text = value
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        payment_type_label.text = "Prepaid"
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            
        }
        
        
        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(KanoElectricity_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == mPIN){
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
            
        else {
            return true
        }
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }
    
    @IBAction func type_Btn(_ sender: UIButton) {
        
        performSegue(withIdentifier: "pop_kano", sender: self)
    }
    
    func Kano_Task(url_: String){
        
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        var responseStr: NSString = ""
        let safeURL = url_.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let request = NSMutableURLRequest(url: URL(string: safeURL!)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    print("\(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                    
                    self.mMeterNO.text! = ""
                    self.mAmount.text! = ""
                    self.mPIN.text! = ""
                }
                
            }
            
            
        }
        
        task.resume()

    }
    
    
    func Verify_Task(url_: String){
        
        var customer_name : NSString = ""
        var trans_charge : NSString = ""
        
        var tarrif_code : NSString = ""
        var tarrif_rate : NSString = ""
        
        var min_purchase : NSString = ""
        var customer_arrears : NSString = ""
        var business_unit: NSString = ""
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    print("\(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    //responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
                    
                    self.indicator.stopAnimating()
                    
                    
                    if(responseStr.contains("|")) { // expected response
                        
                        let seperated_String = responseStr.components(separatedBy: "|")
                        
                        trans_charge = seperated_String[0] as NSString
                        customer_name = seperated_String[1] as NSString
                        
                        min_purchase = seperated_String[2] as NSString
                        customer_arrears = seperated_String[3] as NSString
                        
                        tarrif_code = seperated_String[4] as NSString
                        tarrif_rate = seperated_String[5] as NSString
                        business_unit = seperated_String[6] as NSString
                        
                        
                        if(customer_name.contains("200") ){
                            
                            customer_name = "Unavailable"
                        }
                        
                        // display a dialog for the user to verify the acc
                        let total = (trans_charge as NSString).integerValue + (self.Amount as NSString).integerValue
                        
                        let msg = "Customer Name : \(customer_name)\n\nMeter NO : \(self.MeterNo)\n\nType : \(self.payment_type)\n\nMinimum Purchase : \(min_purchase)\n\nArrears : \(customer_arrears)\n\nTarrif Code : \(tarrif_code)\n\nTarrif Rate : \(tarrif_rate)\n\nAmount : ₦\(self.Amount)\n\nTransaction Charge : ₦\(trans_charge)\n\nTotal : ₦\(total)"
                        
                        let controller = UIAlertController(title: "Transaction Details", message: msg, preferredStyle: UIAlertControllerStyle.alert)
                        let cancelAction = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil)
                        let ActionButton = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.destructive, handler: { action in
                            
                            
                            let encodedMobile = UtilHelpers().toBase64(self.db_MobileNo as String)
                            let encodedPIN = UtilHelpers().toBase64(self.PIN as String)
                            
                            
                            let kano_url = Constants.BASE_URL+"method=kedcopayment&customerid=\(self.MeterNo as String)&amount=\(self.Amount as String)&metertype=\(self.payment_type as String)&mobile=\(encodedMobile)&businessUnit=\(business_unit as String)&pin=\(encodedPIN)"
                            
                            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                                
                                self.Kano_Task(url_: kano_url)
                            }
                                
                            else {
                                
                                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                                
                            }
                            
                        })
                        
                        controller.addAction(cancelAction)
                        controller.addAction(ActionButton)
                        self.present(controller, animated: true, completion: nil)
                        
                    }
                    
                    else if(responseStr.hasPrefix("|0||400")) { // must return a count of 4, o
                        
                        print("here in 400")
                        UtilHelpers().displayAlert("FAILED", msg: "Please Check Your Meter ID")
                        
                    }
                    
                    
                    else {
                        
                        
                        UtilHelpers().displayAlert("Response", msg: responseStr as String)
                        
                    }
                    
                    
                }

                
            }
            
            
        }
        
        task.resume()
    
        
    }
    
    
    
    
    
    
    
    
    
    @IBAction func Kano_Btn(_ sender: UIButton) {
        
        
        mMeterNO.resignFirstResponder()
        mAmount.resignFirstResponder()
        mPIN.resignFirstResponder()
        
        MeterNo = mMeterNO.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        Amount = mAmount.text! as NSString
        PIN  = mPIN.text! as NSString
        payment_type = payment_type_label.text! as NSString
        
        if(mMeterNO.text!.isEmpty && mAmount.text!.isEmpty && mPIN.text!.isEmpty){
            
            UtilHelpers().displayAlert("ERROR", msg: "Insert the required details into text fields")
        }
            
        else if(MeterNo.length == 0) {
            
            UtilHelpers().displayAlert("ERROR!", msg: "Meter Number Is Required")
        }
            
        else if(PIN.length == 0){
            UtilHelpers().displayAlert("ERROR!", msg: "4 Digit PIN Required")
            
        }
            
        else if(Amount.length == 0){
            
            UtilHelpers().displayAlert("ERROR", msg: "Amount Is Required")
            
        }
            
        else if(PIN.length < 4){
            
            UtilHelpers().displayAlert("ERROR!", msg: "4 Digit PIN Required")
            
        }
            
            
        else {
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                let phcn_charge_uri = Constants.BASE_URL+"method=kedcoenquiry&meterType=\(payment_type as String)&customerid=\(self.MeterNo as String)"
                
                Verify_Task(url_: phcn_charge_uri)
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            
            switch (identifier){
                
                case "pop_kano":
                
                    if let vc = segue.destination as? Options_TableViewController {
                        
                        vc.table_items = ["Prepaid","Postpaid"]
                        
                        vc.delegate = self
                    }
            default :
                break;
            }
        }
    }
}
