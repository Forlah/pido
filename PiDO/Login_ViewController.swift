//
//  Login_ViewController.swift
//  PiDO
//
//  Created by sp developer on 11/21/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    open override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        
        // return (visibleViewController?.supportedInterfaceOrientations())!
        if visibleViewController is Login_ViewController {
            return UIInterfaceOrientationMask.portrait
        }
        
        else if(visibleViewController is MenuTableViewController){
            
            return UIInterfaceOrientationMask.portrait
        }
        
        else if(visibleViewController is MenuTableViewController || visibleViewController is AirtimeTopup_ViewController || visibleViewController is SendMoney_ViewController || visibleViewController is Cashout_ViewController || visibleViewController is ChangePin_ViewController || visibleViewController is PidoContacts_ViewController || visibleViewController is ForgotPassword_ViewController || visibleViewController is Paybills_ViewController){
            
            return UIInterfaceOrientationMask.portrait
        }
        
        return UIInterfaceOrientationMask.portrait
    }
}

class Login_ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var mPhoneNumber: UITextField!
    @IBOutlet weak var mPassword: UITextField!
    
    var Password : NSString = ""
    var PiDOMobile : NSString = ""
    var mPhone = ""
    let AppVersion = "iOS1.0"
    var indicator : ActivityIndicator!
    
    
    var db_MobileNo : String? = ""
    var db_UniqueID = ""
    var db_token = ""
    var db_Password = ""
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
      override func viewDidLoad() {
        super.viewDidLoad()
        
        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(Login_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
        let db_Reg = PiDOUser_DB().getAll()
        
        if(PiDOUser_DB().isExisting() == true) {
            
            db_MobileNo = db_Reg[0].getMobileNo()
            db_UniqueID = db_Reg[0].getUniqueID()
            db_Password = db_Reg[0].getPassword()
            db_token = db_Reg[0].getToken()
            
            if(db_MobileNo != ""){
                
                mPhoneNumber.text = db_MobileNo!
            }
            
        }
        
        
        if(mPhone != "") {
            mPhoneNumber.text = mPhone
        }
        
        
    }
    
    
    func Login_task(url_: String) {
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){data, response, error in
            
            DispatchQueue.main.async { // main thread
                
                if let error = error {
                    print( (error))
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    self.indicator.stopAnimating()
                    print("Response from service : \(responseStr)")
                    
                    if (responseStr.contains("8001") == true ){
                        self.mPassword.text = ""
                        // segue to MenuTableView Controller viewcontroller
                        self.performSegue(withIdentifier: "login", sender: self)
                        
                        
                    }
                        
                    else if(responseStr.contains("8007")){
                        
                        // display a dialog for user to change pin
                        let controller2 = UIAlertController(title: "INFORMATION", message: "Please Kindly Change Your PIN", preferredStyle: UIAlertControllerStyle.alert)
                        
                        let ActionButton = UIAlertAction(title: "OK",style: .default, handler: { action in
                            
                            self.performSegue(withIdentifier: "login", sender: self)
                            
                            
                        })
                        
                        controller2.addAction(ActionButton)
                        self.present(controller2, animated: true, completion: nil)
                        
                        
                    }
                        
                    else {
                        
                        UtilHelpers().displayAlert("LOGIN FAILED ", msg: responseStr as String)
                        
                    }
                    
                }
                
            }
            
            
        }
        task.resume()
        
    }
    
    

    @IBAction func Login_Btn(_ sender: UIButton) {
        
      //  performSegueWithIdentifier("login", sender: self)
        
        if(db_MobileNo == "" && db_UniqueID == "" && db_Password == "" && db_token == "" ) {
            var db_Reg = PiDOUser_DB().getAll()
            db_Password = db_Reg[0].getPassword()
            db_UniqueID = db_Reg[0].getUniqueID()
            db_token = db_Reg[0].getToken()
            db_MobileNo = db_Reg[0].getMobileNo()
            
        }
        
        PiDOMobile = mPhoneNumber.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        Password = mPassword.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        
        mPhoneNumber.resignFirstResponder()
        mPassword.resignFirstResponder()
        
               
        if(PiDOMobile.length == 0 && Password.length == 0){
            
              UtilHelpers().displayAlert("ERROR!", msg: "Insert The Required Details Into Text Fields")
        }
        
        else if(PiDOMobile.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "PiDO Mobile Number Is Required")
        }
        
        else if(PiDOMobile.length < 11){
             UtilHelpers().displayAlert("ERROR!", msg: "11 Digit PiDO Mobile Number Is Required")
        }
        
        else if(Password.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Password Is Required")
            
        }
        
        else if(Password.length < 6){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Password must be a minimum of 6 characters");
        }
            
            // compare password form db and from textfield
            
        else if(Password.isEqual(to: db_Password) ==  false || PiDOMobile.isEqual(to: db_MobileNo!) == false){
            
            UtilHelpers().displayAlert("LOGIN FAILED ", msg: "Invalid Login Credentials");
            
        }
        
        
        else {
            
            
                
                let encodedMsisdn = UtilHelpers().toBase64(PiDOMobile as String)
                let encodedUniqueId = UtilHelpers().toBase64(db_UniqueID)
                
               let login_url =  Constants.BASE_URL+"method=login&msisdn=\(encodedMsisdn)&password=\(Password)&autcode=\(encodedUniqueId)&device=iOS&token=\(db_token)&version=\(AppVersion)"
                
                if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                    
                    Login_task(url_: login_url)
                    
                }
                    
                else {
                    
                    UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                    
                }
            
            
        }
        
       
    }
    
    @IBAction func Btn_forgotpassword(_ sender: UIButton) {
        performSegue(withIdentifier: "forgotpassword", sender: self)
    }
    
   
    
     //responsible for removing the keyboard from screen when the return key is pressed.
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }
    
    
           /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
