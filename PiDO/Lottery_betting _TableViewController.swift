//
//  Lottery_betting _TableViewController.swift
//  PiDO
//
//  Created by sp developer on 12/13/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

class Lottery_betting__TableViewController: UITableViewController {
    
     fileprivate let titles = [("Bet9ja","bet9ja")]
    
    
    func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }

    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return titles.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "lottery_cells", for: indexPath) 

        // Configure the cell...
        let (title,image) = titles[indexPath.row]
        
        cell.textLabel?.text = title
        
        let menu_item_image = UIImage(named: image)
        
        let idiom = UIDevice.current.userInterfaceIdiom // check if current device is an ipad or iphone
        
        
        if idiom == UIUserInterfaceIdiom.phone{
            
            let resized_image = resizeImage(menu_item_image!,newWidth: 40)
            
            cell.imageView?.image = resized_image
            
            
            
        }
            
        else{
            let resized_image = resizeImage(menu_item_image!,newWidth: 50)
            
            cell.imageView?.image = resized_image
            
            
        }
        

        

        return cell
    }
    
    
    
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            performSegue(withIdentifier: "bet9ja", sender: nil)
        }
        
    }
    
    
    
    

    /*    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
