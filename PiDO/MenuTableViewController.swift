//
//  MenuTableViewController.swift
//  PiDO
//
//  Created by sp developer on 11/21/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

@objc protocol updater {
    
    func fromService(_ value:String)
}

class MenuTableViewController: UITableViewController, updater {
    
    fileprivate let titles_descriptions = [("Airtime Topup","Recharge MTN, Airtel Etisalat and Glo airtime","airtime_"),("Send Money","To PiDO, to bank or other mobile money","send_money"), ("Cash Out","Receive cash from a nearby agent","cash_out"), ("Pay Bills","Pay for goods and services","pay_bills"), ("Change PIN","Change your PiDO pin","change_pin"),("Transaction History","Check previous transactions","transaction_history"), ("PiDO Contact","Manage or send money to contacts","pido_contact")]
    
    var refresh_Control : UIRefreshControl!
    
    var data = NSMutableData()
    
    var Contacts_DB_status : Bool = false
    
    var  db_MobileNo = ""
    

    fileprivate var x = ""
    var balance = "please wait.."
    
    
    func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            
           
        }
        
        // setup refresh control
        refresh_Control = UIRefreshControl()
        
        refresh_Control.attributedTitle = NSAttributedString(string: " Refreshing....")
        refresh_Control.addTarget(self, action: #selector(MenuTableViewController.refresh), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refresh_Control)
        getBalance()
        

    }
    
    func fromService(_ value: String) {
        
    }
    
    func refresh(){
        getBalance()
        
    }
    
    
       // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        
        return titles_descriptions.count
        

    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cells", for: indexPath) 
        
        
        let (title,desc,image) = titles_descriptions[ indexPath.row]
        
        cell.textLabel?.text = title
        
        cell.detailTextLabel?.text = desc
        
        
        
        // get image
//       let imageViewObject = UIImageView(frame:CGRectMake(0, 0, 20, 20))
//
//        
//        imageViewObject.image = UIImage(named: mIMAGE)
//        cell.imageView?.image = imageViewObject.image
                   let menu_item_image = UIImage(named: image)
        
        let idiom = UIDevice.current.userInterfaceIdiom // check if current device is an ipad or iphone
        
        
        if idiom == UIUserInterfaceIdiom.phone{
            
            let resized_image = resizeImage(menu_item_image!,newWidth: 40)
            
            cell.imageView?.image = resized_image
            


        }
        
        else{
            let resized_image = resizeImage(menu_item_image!,newWidth: 50)
            
            cell.imageView?.image = resized_image
            

        }
        
        
        
        
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
          return "Account Balance: \(balance)"
        
    }
    
    
    
    @IBAction func unwindToMainMenu(segue: UIStoryboardSegue) { // unwind back to mainmenu for very first time
        
        tableView.reloadData() // reload tableview datasource
        
    }

    
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
            
        {
            // segue to airtime viewcontroller class
            performSegue(withIdentifier: "airtime", sender: nil)
            
        }
            
            
            
        else if(indexPath.row == 1) {
            
            // segue to sendmoney viewcontroller class
            
            performSegue(withIdentifier: "sendmoney", sender: nil)
            
        }
            
            
            
        else if(indexPath.row == 2){
            
            performSegue(withIdentifier: "cashout", sender: nil)
            
        }
            
            
            
        else if(indexPath.row == 3){
            
            performSegue(withIdentifier: "paybills", sender: nil)
            
        }
            
            
            
        else if(indexPath.row == 4){
            
            performSegue(withIdentifier: "changepin", sender: nil)
            
        }
            
            
        else if(indexPath.row == 5){
            performSegue(withIdentifier: "history", sender: nil)
            
        }
            
            
            
        else if (indexPath.row == 6){
            
            Contacts_DB_status = Contacts_DB().isExisting()

            
            if(Contacts_DB_status == false) { // check if contacts table exist
                
                performSegue(withIdentifier: "empty_contact", sender: self)
            }
            
            else {
                performSegue(withIdentifier: "contacts", sender: self)
                
            }
            
           
        }
        
        
    }
    
    func getBalance() -> Void { // api call for account balance
        
        let urlString = Constants.BASE_URL+"method=balance2&mobile=\(db_MobileNo)"
        let request = NSMutableURLRequest(url: URL(string: urlString)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                if(data!.count < 0){
                    self.balance = "Not available"
                    
                    // dismis refreshing animation
                    if self.refresh_Control.isRefreshing{
                        
                        self.refresh_Control.endRefreshing()
                    }
                    
                    
                    self.tableView.reloadData() // reload tableview with new data
                    
                    
                }
                    
                else{
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    // converting
                    let responseStr : String = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String
                    
                    let styler = NumberFormatter()
                    styler.minimumFractionDigits = 2
                    styler.maximumFractionDigits = 2
                    styler.locale = Locale(identifier: "en_NG")
                    styler.numberStyle = .currency
                    let converter = NumberFormatter()
                    converter.decimalSeparator = "."
                    if let result = converter.number(from: responseStr) {
                        let value = styler.string(from: result)
                        self.balance = value!
                    }
                        
                    else{
                        self.balance = "Not Available"
                    }
                    
                    // dismis refreshing animation
                    if self.refresh_Control.isRefreshing{
                        
                        self.refresh_Control.endRefreshing()
                    }
                    
                    self.tableView.reloadData() // reload tableview with new data
                    
                    
                    
                }
                
            }
            
           
            
        }
        
        task.resume()

    }
    
    
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
