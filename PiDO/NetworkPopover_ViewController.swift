//
//  NetworkPopover_ViewController.swift
//  PiDO
//
//  Created by sp developer on 11/22/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit
@objc protocol Selected_ValueDelegate {
     func fromPOPover(_ value:String)
    
}

class NetworkPopover_ViewController: UIViewController , UIPickerViewDataSource, UIPickerViewDelegate{
    
    fileprivate let Network_names = ["AIRTEL","9Mobile","GLO","MTN"]
    
    weak var delegate: Selected_ValueDelegate?
    
    @IBOutlet weak var picker: UIPickerView!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        let row = picker.selectedRow(inComponent: 0)
        delegate?.fromPOPover(Network_names[row])


        // Do any additional setup after loading the view.
       // var preferredContentSize = picker.sizeThatFits(presentingViewController!.view.bounds.size)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func Btn_network(_ sender: UIButton) {
        let row = picker.selectedRow(inComponent: 0)
        delegate?.fromPOPover(Network_names[row])
        self.dismiss(animated: true, completion: nil)
    
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return Network_names.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        
        return Network_names[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let row = picker.selectedRow(inComponent: 0)
        delegate?.fromPOPover(Network_names[row])
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
