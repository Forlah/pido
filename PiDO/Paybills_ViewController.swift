//
//  Paybills_ViewController.swift
//  PiDO
//
//  Created by sp developer on 11/21/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

class Paybills_ViewController: UITableViewController {
    
    fileprivate let paybill_titles = [("PayTV","pay_tv"),("PHCN","phcn_"),("Internet Payment","internet_"),("Araya","araya"),("Lottery And Betting","lottery_betting_")]
    
    
    func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    

    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func  tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return paybill_titles.count
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "paytv_cells", for: indexPath) 
        
        let (title,image) = paybill_titles[ indexPath.row]
        
        cell.textLabel?.text = title
        
        let menu_item_image = UIImage(named: image)
        
        let idiom = UIDevice.current.userInterfaceIdiom // check if current device is an ipad or iphone
        
        
        if idiom == UIUserInterfaceIdiom.phone{
            
            let resized_image = resizeImage(menu_item_image!,newWidth: 40)
            
            cell.imageView?.image = resized_image
            
            
            
        }
            
        else{
            let resized_image = resizeImage(menu_item_image!,newWidth: 40)
            
            cell.imageView?.image = resized_image
            
            
        }

        

      //  cell.imageView?.image = menu_item_image
        return cell
        
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            performSegue(withIdentifier: "paytv", sender: self)
            
        }
        
        else if indexPath.row == 1 {
            
            performSegue(withIdentifier: "phcn", sender: self)
            
        }
        
        else if indexPath.row == 2 {
            
            performSegue(withIdentifier: "internetpayment", sender: self)
            
        }
        
        else if indexPath.row == 3 {
            
            performSegue(withIdentifier: "araya", sender: self)
            
        }
            
//        else if indexPath.row == 4 {
//            
//            performSegueWithIdentifier("schoolfees", sender: self)
//            
//        }
        
        else if indexPath.row == 4 {
            
            performSegue(withIdentifier: "lotteryandbetting", sender: self)
            
        }



    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
