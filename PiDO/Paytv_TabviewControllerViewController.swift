//
//  Paytv_TabviewControllerViewController.swift
//  PiDO
//
//  Created by sp developer on 4/12/16.
//  Copyright © 2016 sp developer. All rights reserved.
//

import UIKit

class Paytv_TabviewControllerViewController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        // Do any additional setup after loading the view.
        self.title = "DSTV"
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        self.title = viewController.title
        return true
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
