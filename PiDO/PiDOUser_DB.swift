//
//  PiDOUser_DB.swift
//  PiDO
//
//  Created by sp developer on 1/9/16.
//  Copyright (c) 2016 sp developer. All rights reserved.
//

import Foundation

class PiDOUser_DB {
    
    let DATABASE_NAME : String = "RegistrationManager"
    let DATABASE_TABLE_NAME : String = "PidoUser"
    
    // PidoUser table column names
    let KEY_ID = "ID";
    let KEY_FULLNAME = "FULL_NAME";
    let KEY_MOBILE_NO = "MOBILE";
    let KEY_PASSWORD = "PASSWORD";
    let KEY_AUTHCODE = "UNIQUE_ID";
    let KEY_TOKEN = "TOKEN";
    let KEY_GENDER = "GENDER";
    let KEY_EMAIL = "EMAIL";
    
    func getDatabasePath() -> String {
        
    //let Filemgr = NSFileManager.defaultManager()
        
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        let docDir = dirPaths[0] 
        let databasePath = (docDir as NSString).appendingPathComponent("\(DATABASE_NAME).db")
        
        return databasePath
    }
    
    
    func isExisting() -> Bool {
        
        if getAll().count < 1{
            return false
            
        }
            
            
        else {
            return true
        }
        
        
        
    }
    
    
    
    func createTable() {
        
        let Filemgr = FileManager.default
        let databasePath = getDatabasePath()
        
        
        if Filemgr.fileExists(atPath: databasePath as String) { // checks if file path exists
            
            let database = FMDatabase(path: databasePath as String)
            
            if database == nil {
                print("Error opening database \(DATABASE_NAME)   \(String(describing: database?.lastErrorMessage()))")
            }
            
            
            if(database?.open())! {
                
                let sql_create_stmnt = "CREATE TABLE IF NOT EXISTS \(DATABASE_TABLE_NAME) (\(KEY_ID) INTEGER PRIMARY KEY AUTOINCREMENT, \(KEY_FULLNAME) TEXT, \(KEY_MOBILE_NO) TEXT, \(KEY_PASSWORD) TEXT, \(KEY_AUTHCODE) TEXT, \(KEY_TOKEN) TEXT, \(KEY_GENDER) TEXT, \(KEY_EMAIL) TEXT);"
                
                if !(database?.executeStatements(sql_create_stmnt))! {
                    print("ERROR creating database  \(String(describing: database?.lastErrorMessage()))")
                }
                
                database?.close()
                print("Database created !")
                
            } else {
                print("ERROR could not open database \(String(describing: database?.lastErrorMessage()))")
            }
            
        }
            
        else {
            print("File not existing , database not created")
        }
    }
    
    
    // add pido user credentials to database table
    func savePiDOUser(_ user : User) -> Int{
        
        let database = FMDatabase(path: getDatabasePath() as String)
        
        
        
        if (database?.open())! {
            
            let insert_sql = "INSERT INTO \(DATABASE_TABLE_NAME) (\(KEY_FULLNAME), \(KEY_MOBILE_NO), \(KEY_PASSWORD), \(KEY_AUTHCODE), \(KEY_TOKEN), \(KEY_GENDER), \(KEY_EMAIL)) VALUES ('\(user.getFullname())', '\(user.getMobileNo())', '\(user.getPassword())', '\(user.getUniqueID())', '\(user.getToken())', '\(user.getGender())', '\(user.getEmail())');"
            
            let result =  database?.executeUpdate(insert_sql, withArgumentsIn: nil)
            
            if !result! {
                
                print("Error addding user credentials  \(String(describing: database?.lastErrorMessage()))")
                
                
            }
                
            else {
                print("PiDO User was Added successfully")
                
                return 1
            }
            
        }
            
        else {
            print("Error : \(String(describing: database?.lastErrorMessage()))")
            
            return 0
        }
        
        return 0
    }
    
    
    
    func getAll() -> [User] {
        
        var Items : [User]
        Items = [User]() // intialization
        
        
        let database = FMDatabase(path: getDatabasePath() as String)
        
        if (database?.open())! {
            
            let select_query = "SELECT * FROM \(DATABASE_TABLE_NAME)"
            let results : FMResultSet? = database?.executeQuery(select_query, withArgumentsIn: nil)
            
            
            while (results?.next() == true ){
                
                let id = results?.long(forColumn: "ID")
                let Fullname = results?.string(forColumn: "FULL_NAME")
                let MobileNO =  results?.string(forColumn: "MOBILE")
                let Password =  results?.string(forColumn: "PASSWORD")
                let UniqueID = results?.string(forColumn: "UNIQUE_ID")
                let Token = results?.string(forColumn: "TOKEN")
                let Gender = results?.string(forColumn: "GENDER")
                let Email  = results?.string(forColumn: "EMAIL")
                
                
                
                let _user = User()
                _user.setid(id!)
                _user.setFullname(Fullname!)
                _user.setMobileNo(MobileNO!)
                _user.setPassword(Password!)
                _user.setUniqueID(UniqueID!)
                _user.setToken(Token!)
                _user.setGender(Gender!)
                _user.setEmail(Email!)
                
                Items.append(_user)
            }
            
            database?.close()
            
        }
            
        else {
            print("Error: \(String(describing: database?.lastErrorMessage()))")
        }
        
        
        
        
        return Items
        
    }
    
    
    func updatePiDOUser(_ user : User, id : Int) -> Int {
        
        var result = -1
        let database = FMDatabase(path: getDatabasePath() as String)
        if (database?.open())! {
            
           // let updateQuery = "UPDATE \(DATABASE_TABLE_NAME) SET \(KEY_FULLNAME) = '\(user.getFullname())', \(KEY_MOBILE_NO) = '\(user.getMobileNo())', \(KEY_PASSWORD) = '\(user.getPassword())', \(KEY_AUTHCODE) = '\(user.getUniqueID())', \(KEY_TOKEN) = '\(user.getToken())', \(KEY_GENDER) = '\(user.getGender())', \(KEY_EMAIL) = '\(user.getEmail())', WHERE \(KEY_ID) = '\(id)' "
            
            let update_Query = "UPDATE \(DATABASE_TABLE_NAME) SET \(KEY_PASSWORD) = '\(user.getPassword())' WHERE \(KEY_ID) = '\(id)' "
            
            
            let updateSuccessful =  database?.executeUpdate(update_Query, withArgumentsIn: nil)
            
            if(!updateSuccessful!){
                print("UPDATE FAILED: \(String(describing: database?.lastErrorMessage()))")
                
                result = 0
                return result
            }
                
            else{
                print("UPdate Successful!")
                
                result = 1
                return result
            }
            
            
            
            
        }
        
        
        return 0
    }
    
    
}
