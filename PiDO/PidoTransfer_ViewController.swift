//
//  PidoTransfer_ViewController.swift
//  PiDO
//
//  Created by sp developer on 11/25/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

class PidoTransfer_ViewController: UIViewController , Selected_Contact, UITextFieldDelegate {

    @IBOutlet weak var mRecipient: UITextField!
    @IBOutlet weak var mAmount: UITextField!
    @IBOutlet weak var mPIN: UITextField!
    
    var Recipient : NSString = ""
    var Amount : NSString = ""
    var PIN : NSString = ""
    var indicator : ActivityIndicator!
    var db_MobileNo = ""
    var Contacts_DB_status : Bool = false
    

    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(PidoTransfer_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
        if(Recipient.length != 0){
            
            mRecipient.text = Recipient as String
        }
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            
            
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == mRecipient {
            
            let maxLength = 11
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
        
        else if textField == mPIN {
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
        
        else {
            return true
        }
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }
    
    
    func From_GetPidoContactsTable(_ value: String) {
        mRecipient.text = value
    }
    
    
    func getP2P_charge(url_: String){
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                self.indicator.stopAnimating()
                
                
                if let error = error {
                    
                    print("\(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    if (responseStr.contains("|")){
                        var Acc_Name : String
                        
                        let seperated_String = responseStr.components(separatedBy: "|")
                        
                        let charge  = seperated_String[0]
                        
                        
                        if(seperated_String[1].isEqual("200")){
                            Acc_Name = "Unavailable"
                        }
                            
                        else {
                            
                            Acc_Name = seperated_String[1]
                        }
                        
                        // display a dialog for the user to verify the accname, bank etc
                        
                        self.P2P_Confirmation_Alert(charge, acc_name: Acc_Name)
                        
                        
                        
                    }
                        
                        
                    else {
                        
                        UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                        
                    }
                
            }
                

        }
            
        }
        task.resume()
        
        
        
    }
    
    func PidoTransfer_Task(url_: String){
        
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    print("\(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    self.indicator.stopAnimating() // dismiss activity indicator
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String) // display the response from the service
                    
                    self.mRecipient.text = ""
                    self.mAmount.text = ""
                    self.mPIN.text = ""
                    
                }

                
            }
            
            
        }
        
        
        task.resume()
        
    }
    
    
    func P2P_Confirmation_Alert(_ charge : String , acc_name : String) {
        
        
        let total = (charge as NSString).integerValue + Amount.integerValue
        
        let msg = "Account Name : \(acc_name)\n\nAmount : ₦\(Amount)\n\nTranscation charge : ₦\(charge)\n\nTotal : ₦\(total)"
        
        let controller2 = UIAlertController(title: "Transaction Details", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Dismiss" , style: UIAlertActionStyle.cancel, handler: nil)
        let ActionButton = UIAlertAction(title: "Proceed",style: .destructive, handler: {action in
            
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                let encodedMobileNo = UtilHelpers().toBase64(self.db_MobileNo)
                let encodedPIN = UtilHelpers().toBase64(self.PIN as String)
                let encodedRecipient = UtilHelpers().toBase64(self.Recipient as String)
                
                
                let p2p_transfer_url = Constants.BASE_URL+"method=p2p&phone=\(encodedMobileNo)&amount=\(self.Amount as String)&pin=\(encodedPIN)&recipientmobile=\(encodedRecipient)"
                
                
                self.PidoTransfer_Task(url_: p2p_transfer_url)
                
                
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
            
        })
        
        
        controller2.addAction(cancelAction)
        controller2.addAction(ActionButton)
        self.present(controller2, animated: true, completion: nil)
        
        

        
        
    }
    
    
    
    @IBAction func GetPidoContacts_Btn(_ sender: UIButton) {
        
        Contacts_DB_status = Contacts_DB().isExisting()
        
        
        if(Contacts_DB_status == false) {
            
            UtilHelpers().displayAlert("No Content",msg: "Your PiDO Contact list is empty")
        }
            
        else {
            performSegue(withIdentifier: "get_contacts", sender: self)
            
        }
        

        
    }
    
    
    @IBAction func PidoTransfer_Btn(_ sender: UIButton) {
        
        Recipient = mRecipient.text! as NSString
        Amount = mAmount.text! as NSString
        PIN = mPIN.text! as NSString
        
        mRecipient.resignFirstResponder()
        mAmount.resignFirstResponder()
        mPIN.resignFirstResponder()
        
        if(mRecipient.text!.isEmpty && mAmount.text!.isEmpty && mPIN.text!.isEmpty){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Insert the required details into text fields ")
        }
        
        
        else if Recipient.length == 0 || Recipient.length < 11 {
            
            UtilHelpers().displayAlert("ERROR!", msg: "11 Digit Recipient Phone Number Required ")
        }
        
        else if Amount.length == 0 {
             UtilHelpers().displayAlert("ERROR!", msg: "Amount Text Field Required")
        }
        
        else if PIN.length == 0 || PIN.length < 4{
            
            UtilHelpers().displayAlert("ERROR!", msg:"4 Digit PIN Required")
        }
            
        
        else {
            
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                
                let P2P_charge_url = Constants.BASE_URL+"method=chargesp2p&amount=\(Amount as String)&msisdn=\(Recipient as String)"
                
                getP2P_charge(url_: P2P_charge_url)
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            
            switch identifier {
                
                case "get_contacts":
                if let vc = segue.destination as? GetPidoContacts_TableTableViewController{
                    vc.delegate = self
                }
                
                default:
                break
            }
        }

    }

}
