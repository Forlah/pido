//
//  PostPaid_ViewController.swift
//  PiDO
//
//  Created by sp developer on 1/30/16.
//  Copyright © 2016 sp developer. All rights reserved.
//

import UIKit

class PostPaid_ViewController: UIViewController, UITextFieldDelegate {

    var indicator : ActivityIndicator!
    
    @IBOutlet weak var mMeterNO: UITextField!
    @IBOutlet weak var mAmount: UITextField!
    @IBOutlet weak var mPIN: UITextField!
    
    let ProductId = "33"
    var MeterNo : NSString = ""
    var PIN : NSString = ""
    var Amount : NSString = ""
    
    var db_MobileNo = ""
    
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            
        }
        
        
        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(PostPaid_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == mPIN){
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
            
        else {
            return true
        }
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }
    
    
    func Postpaid_Task(url_: String){
        
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){data, response, error in
            
            if let error = error {
                print( (error))
                
                responseStr = "Unable to connect to the service at the moment, Please try again later"
                self.indicator.stopAnimating()
                
                UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
            }
                
            else {
                
                responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                
                self.indicator.stopAnimating()
                
                
                UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                
                
                self.mMeterNO.text! = ""
                self.mAmount.text! = ""
                self.mPIN.text! = ""
            }
        }
        task.resume()
        
    }
    
    
    func Verify_Task(url_: String){
        
        var customer_name : NSString = ""
        var trans_charge : NSString = ""
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){data, response, error in
            
            if let error = error {
                print( (error))
                
                responseStr = "Unable to connect to the service at the moment, Please try again later"
                self.indicator.stopAnimating()
                
                UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
            }
                
            else {
                
                print("\(data!.count) bytes of data was returned")
                
                responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                
                self.indicator.stopAnimating()
                print("Response from service : \(responseStr)")
                
                self.indicator.stopAnimating()
                
                print("Response from service : \(responseStr)")
                
                if(responseStr.hasPrefix("|0||400")) { // must return a count of 4, o
                    
                    print("here in 400")
                    UtilHelpers().displayAlert("FAILED", msg: "Please Check Your ID")
                    
                }
                    
                else if(responseStr.contains("|")){
                    
                    
                    let seperated_String = responseStr.components(separatedBy: "|")
                    
                    customer_name = seperated_String[0] as NSString
                    trans_charge = seperated_String[1] as NSString
                    
                    
                    if(customer_name.contains("200") ){
                        
                        customer_name = "Unavailable"
                    }
                        
                        
                    else {
                        
                        self.indicator.stopAnimating() // dismiss indicator
                        
                        
                        // display a dialog for the user to verify the acc
                        let total = (trans_charge as NSString).integerValue + (self.Amount as NSString).integerValue
                        
                        let msg = "Customer Name : \(customer_name)\n\nMeter NO : \(self.MeterNo)\n\nAmount : ₦\(self.Amount)\n\nTransaction Charge : ₦\(trans_charge)\n\nTotal : ₦\(total)"
                        
                        let controller = UIAlertController(title: "Transaction Details", message: msg, preferredStyle: UIAlertControllerStyle.alert)
                        let cancelAction = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil)
                        let ActionButton = UIAlertAction(title: "Proceed", style: UIAlertActionStyle.destructive, handler: { action in
                            
                            
                            let encodedMobile = UtilHelpers().toBase64(self.db_MobileNo as String)
                            let encodedPIN = UtilHelpers().toBase64(self.PIN as String)
                            
                            
                            let postpaid_url = Constants.BASE_URL+"method=payphcn&meterno=\(self.MeterNo)&mobile=\(encodedMobile)&pin=\(encodedPIN)&productid=\(self.ProductId)&charge=\(trans_charge)"
                            
                            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                                
                                self.Postpaid_Task(url_: postpaid_url)
                            }
                                
                            else {
                                
                                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                                
                            }
                            
                            
                        })
                        
                        
                        controller.addAction(cancelAction)
                        controller.addAction(ActionButton)
                        self.present(controller, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                    
                    
                else {
                    
                    
                    UtilHelpers().displayAlert("Response", msg: responseStr as String)
                    
                }
            }
            
        }
        task.resume()
        
    }
    
    

    
    
    
    @IBAction func Postpaid_Btn(_ sender: UIButton) {
        
        
        mMeterNO.resignFirstResponder()
        mAmount.resignFirstResponder()
        mPIN.resignFirstResponder()
        
        MeterNo = mMeterNO.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        Amount = mAmount.text! as NSString
        PIN  = mPIN.text! as NSString
        
        if(mMeterNO.text!.isEmpty && mAmount.text!.isEmpty && mPIN.text!.isEmpty){
            
            UtilHelpers().displayAlert("ERROR", msg: "Insert the required details into text fields")
        }
            
        else if(MeterNo.length == 0) {
            
            UtilHelpers().displayAlert("ERROR!", msg: "Meter Number Is Required")
        }
            
        else if(PIN.length == 0){
            UtilHelpers().displayAlert("ERROR!", msg: "4 Digit PIN Required")
            
        }
            
        else if(Amount.length == 0){
            
            UtilHelpers().displayAlert("ERROR", msg: "Amount Is Required")
            
        }
            
        else if(PIN.length < 4){
            
            UtilHelpers().displayAlert("ERROR!", msg: "4 Digit PIN Required")
            
        }
            
            
        else {
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                let phcn_charge_uri = Constants.BASE_URL+"method=phcnenquiry&meterno=\(MeterNo)"
                
                Verify_Task(url_: phcn_charge_uri)
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
            
        }
        
        
    }
}
