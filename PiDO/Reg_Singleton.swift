//
//  Reg_Singleton.swift
//  PiDO
//
//  Created by sp developer on 1/7/16.
//  Copyright (c) 2016 sp developer. All rights reserved.
//

import Foundation

// singleton class that holds the registration textfields data

class Reg_Singleton {
    
    static let sharedInstance = Reg_Singleton()
    var mData : Data =  Data()
    
    var Firstname : String = ""
    var Surname : String = ""
    var Gender : String = ""
    
    var PiDOMobile: String = ""
    var Email : String = ""
    var Password : String = ""
    var AuthCode : String = ""
    var token : String = ""
    
    
    // methods
    func getDetails() -> Data {
        
        return mData
        
    }
    
    
    
    
    func addDetails(_ firstname : String, surname : String, gender : String, pidoMobile : String,
        email : String, password : String , authcode : String, token : String){
        
            mData.setFirstname(firstname)
            mData.setSurname(surname)
            mData.setGender(gender)
            mData.setPidoMobile(pidoMobile)
            mData.setEmail(email)
            mData.setPassword(password)
            mData.setAuthcode(authcode)
            mData.setToken(token)
            
            
    }
    
    
    
    
    
    
    
}
