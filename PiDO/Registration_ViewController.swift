//
//  Registration_ViewController.swift
//  PiDO
//
//  Created by sp developer on 12/15/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

class Registration_ViewController: UIViewController, UITextFieldDelegate,UIPopoverPresentationControllerDelegate, Selected_Option {
    
    var isExisting : Bool = true
    
    var Firstname : NSString = ""
    var Surname : NSString = ""
    var Gender : NSString = ""
    var PiDOMobile : NSString = ""
    var Email : NSString = ""
    var Password : NSString = ""
    var ConfirmPassword : NSString = ""
    
    let AppVersion = Constants.AppVersionCode
    var UniqueID : String = ""
    var sQuestion : String = ""
    

    @IBOutlet weak var gender_label: UITextField!
    @IBOutlet weak var mFirstname: UITextField!
    @IBOutlet weak var mPiDOMobile: UITextField!
    @IBOutlet weak var mEmail: UITextField!
    @IBOutlet weak var mSurname: UITextField!
    @IBOutlet weak var mPassword: UITextField!
    @IBOutlet weak var mConfirmPassword: UITextField!
    
    var indicator : ActivityIndicator!
    
    
    func fromTableview(_ value: String) {
        
        gender_label.text = value
    }
    
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
    
    func isValidEmail(_ testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    // fuction to display alert to the UI
    func displayAlert(_ mTitle : String, msg : String) -> Void{
        let alertView : UIAlertView = UIAlertView()
        alertView.title = mTitle
        alertView.message = msg
        alertView.delegate = self
        alertView.addButton(withTitle: "OK")
        alertView.show()
    }
    
        func pop(){
                  }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(Registration_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
               // gender_label.addTarget(self, action: "pop", forControlEvents: UIControlEvents.TouchDown)
        
    }

   
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == mPiDOMobile{
            
            let maxLength = 11
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
            
       
        
        return true
    }

    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == gender_label {
            
           // hide gender textfield keyboard when pressed
          return false
        }
        
        else {
            return true
        }
        
        
    }
    
    func check_updateDB_task(url_: String , Token_Str: String){
        
        var responseStr: NSString = ""
        let safeUrl = URL(string: url_.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
        let request = NSMutableURLRequest(url: safeUrl!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
    
                
                if let error = error{
                    print("failure \(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    
                    responseStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                    print("server response = \(responseStr)")
                    
                    self.indicator.stopAnimating()
                    
                    print("Response from service : \(responseStr)")
                    
                    if (responseStr.contains("1") == false){
                        
                        
                        UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                        
                        
                    }
                        
                    else {
                        // save the credentials of the user in the singleton class before leaving the registration viewcontroller class
                        
                        Reg_Singleton.sharedInstance.addDetails(self.Firstname as String, surname: self.Surname as String, gender: self.Gender as String, pidoMobile: self.PiDOMobile as String, email: self.Email as String, password: self.Password as String, authcode: self.UniqueID, token: Token_Str)
                        
                        self.performSegue(withIdentifier: "existing_user", sender: self)
                        
                    }
                    
            
                    
                    
                    
                }
            
            }
                
            
            
        }
        task.resume()
    }
    
    
    func CheckUser_task(url_: String) -> Void {
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    print("\(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    if (responseStr.contains("|")){
                        
                        let seperated_String = responseStr.components(separatedBy: "|")
                        
                        self.sQuestion = seperated_String[0]
                        
                        let token = seperated_String[1]
                        
                        self.UniqueID = "MBIOS\(self.PiDOMobile as String)" // unique id to identify users on IOS only
                        
                        let Password_str = self.Password.trimmingCharacters(in: CharacterSet.whitespaces) // trim password of white spaces
                        
                        let check_update_uri  = Constants.BASE_URL+"method=checkandupdate&mobile=\(self.PiDOMobile as String)&autcode=\(self.UniqueID)&version=\(self.AppVersion)&password=\(Password_str)"
                        
                        
                        if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                            
                            self.check_updateDB_task(url_: check_update_uri, Token_Str: token)
                            
                        }
                            
                        else {
                            
                            self.indicator.stopAnimating()
                            
                            UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                            
                        }
                        
                        
                        
                    }
                        
                        // if content of result is ERROR, user doesn't exist
                    else if(responseStr.contains("ERROR")){
                        
                        // save the credentials of the user in the singleton class before leaving the viewcontroller registration class
                        
                        // NOte: no token for new user
                        
                        self.indicator.stopAnimating()
                        
                        
                        Reg_Singleton.sharedInstance.addDetails(self.Firstname as String, surname: self.Surname as String, gender: self.Gender as String, pidoMobile: self.PiDOMobile as String, email: self.Email as String, password: self.Password as String, authcode: self.UniqueID, token: "")
                        
                        self.performSegue(withIdentifier: "new_user", sender: self)
                        
                        
                    }
                        
                        
                    else {
                        self.indicator.stopAnimating()
                        
                        UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                        
                    }
                    
                    
                }
                
                
                
            }
            
            
        }
        
        task.resume()
        
    }
    
    
    @IBAction func Gender_Btn(_ sender: UIButton) {
        
        performSegue(withIdentifier: "pop_gender", sender: self)

    }
    
       @IBAction func Reg_Btn(_ sender: UIButton) {
        
       // self.performSegueWithIdentifier("existing_user", sender: self)
        

        Firstname = mFirstname.text! as NSString
        Surname = mSurname.text! as NSString
        Gender = gender_label.text! as NSString
        if Gender.length == 0 {
            let gender = "Other" as NSString
            Gender = gender
        }
        Email = mEmail.text! as NSString
        PiDOMobile = mPiDOMobile.text! as NSString
        Password = mPassword.text! as NSString
        ConfirmPassword = mConfirmPassword.text! as NSString
        
        mFirstname.resignFirstResponder()
        mSurname.resignFirstResponder()
        mEmail.resignFirstResponder()
        mPiDOMobile.resignFirstResponder()
        mPassword.resignFirstResponder()
        mConfirmPassword.resignFirstResponder()
        
        
        
        if (mFirstname.text!.isEmpty && mSurname.text!.isEmpty && mEmail.text!.isEmpty &&
            mPiDOMobile.text!.isEmpty && mPassword.text!.isEmpty && mConfirmPassword.text!.isEmpty) {
                
                UtilHelpers().displayAlert("ERROR!", msg: "Insert the required details into text fields")
                
        }
            
        else if(Firstname.length == 0) {
            
            UtilHelpers().displayAlert("ERROR!", msg: "Firstname Is Required")
            
        }
            
//        else if(Gender.length == 0){
//            
//            UtilHelpers().displayAlert("ERROR!", msg: "Please Select Your Gender")
//            
//        }
        
        else if(Surname.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Surname Is Required")
        }
        
        else if(PiDOMobile.length == 0){
            UtilHelpers().displayAlert("ERROR!", msg: "PiDO Mobile Number Is Required")
            
        }
        
        else if(Email.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Email Is Required")
            
        }
        
        else if(!isValidEmail(Email as String)){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Please Verify Email Addresss")
            
        }
        
        else if(Password.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Password Is Required")
            
        }
        
        else if(Password.length < 6){
          
            UtilHelpers().displayAlert("ERROR!", msg: "Password must be a minimum of 6 characters")

        }
        
       
        else if(ConfirmPassword.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Confirm Password Is Required")

        }
        
        else if(ConfirmPassword.isEqual(to: Password as String) == false){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Passwords Do Not Match")

        }
        
        else {
          
            
            let test_url = Constants.BASE_URL+"method=test&fone=\(PiDOMobile as String)"
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                CheckUser_task(url_: test_url)
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
            
        }
        
        
        
    }
    
    
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            
            switch (identifier) {
                
            case "pop_gender":
                
                if let vc = segue.destination as? Options_TableViewController {
                    
                    vc.table_items =   ["Male","Female","Other"]
                    
                    vc.delegate = self
                }
                
            case "existing_user":
                
                
                if let Vc = segue.destination as? SecurityQnA {
                    
                    
                    Vc.PiDO_sQuestion = self.sQuestion
                    
                }
                
                
            default:
                break;
            }
            
        }
        
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        
        return UIModalPresentationStyle.none
    }
    
    

}

