//
//  SecurityQnA.swift
//  PiDO
//
//  Created by sp developer on 12/28/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

class SecurityQnA: UIViewController, UITextFieldDelegate {
    
    var sAnswer : NSString = ""
    let AppVersion = "iOS1.0"
    let device = "iOS"
    
    var PiDO_sQuestion : String = ""
    var indicator : ActivityIndicator!
    

    @IBOutlet weak var mSQuestion: UITextField!
    @IBOutlet weak var mSAnswer: UITextField!
    
    var Firstname = ""
    var Surname = ""
    var UniqueID = ""
    var Gender = ""
    var Password = ""
    var PiDOMobile_No = ""
    var Token = ""
    var Email = ""
    
    var mPhone =  ""
    
    
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboard
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(SecurityQnA.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
        print("Pido Security Question from Reg = \(PiDO_sQuestion)")

        
        if(PiDO_sQuestion != "") {
            
            mSQuestion.text = PiDO_sQuestion
            
        }
        
        else {
            mSQuestion.text = "Security Question Not Found, Please try agin."
        }
        
        
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }
    
    func loginWith_Security(url_: String){
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    print( (error))
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                }
                    
                else {
                    self.indicator.stopAnimating()

                    print("\(data!.count) bytes of data was returned")
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    if (responseStr.contains("8001") == true ){
                        
                        // retrieve the credential from the singleton class and save user credential to sqlite database
                        
                        let data = Reg_Singleton.sharedInstance.getDetails()
                        let user = User()
                        
                        let mFullname = data.getSurname().trimmingCharacters(in: CharacterSet.whitespaces) + "  " + data.getFirstname().trimmingCharacters(in: CharacterSet.whitespaces)
                        
                        user.setFullname(mFullname)
                        user.setMobileNo(data.getPidoMobile())
                        user.setPassword(data.getPassword().trimmingCharacters(in: CharacterSet.whitespaces))
                        user.setUniqueID(data.getAuthCode())
                        user.setToken(data.getToken().trimmingCharacters(in: CharacterSet.whitespaces))
                        user.setGender(data.getGender())
                        user.setEmail(data.getEmail())
                        
                        // create and save to database
                        let pidouser_db = PiDOUser_DB()
                        
                        pidouser_db.createTable()
                        
                        let status = pidouser_db.savePiDOUser(user)
                        
                        if(status == 1){
                            
                            self.indicator.stopAnimating()
                            
                            // segue to login viewcontroller
                            self.performSegue(withIdentifier: "existing_user to login", sender: self)
                            
                        }
                            
                        else {
                            
                            self.indicator.stopAnimating()
                            
                            UtilHelpers().displayAlert("INFORMATION", msg: "Unable To Save User Credentials")
                            
                            // segue to login viewcontroller
                            self.performSegue(withIdentifier: "existing_user to login", sender: self)
                            
                        }
                        
                        
                    }
                        
                    else if(responseStr.contains("8002")){
                        
                        UtilHelpers().displayAlert("RESPONSE", msg: "Invalid Credentials")
                        
                    }
                        
                    else {
                        
                        UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                        
                    }
                    
                }
                
            }
            
            
        
        }
        task.resume()
        
    }
    
    
    @IBAction func SecurityQnA_Btn(_ sender: UIButton) {
        mSAnswer.resignFirstResponder()
        
        sAnswer = mSAnswer.text! as NSString
        
       // performSegueWithIdentifier("existing_user to login", sender: self)

        
        if(sAnswer.length == 0){
            UtilHelpers().displayAlert("ERROR", msg: "Security Answer Is Required")
        }
        
        else {
            let data = Reg_Singleton.sharedInstance.getDetails()
            
             Firstname = data.getFirstname()
             Surname = data.getSurname()
             UniqueID = data.getAuthCode()
             Gender =  data.getGender()
             Password = data.getPassword()
             PiDOMobile_No = data.getPidoMobile()
             Token =  data.getToken()
             Email = data.getEmail()
            
            // trim out spaces
            let Answer = sAnswer.trimmingCharacters(in: CharacterSet.whitespaces)
            
            
            let loginwith_securityquestion_url : String = Constants.BASE_URL+"method=loginwithsecurityquestion&msisdn=\(PiDOMobile_No)&password=\(Password)&autcode=\(UniqueID)&device=\(device)&securityanswer=\(Answer)&version=\(AppVersion)"
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                loginWith_Security(url_: loginwith_securityquestion_url)
                
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
        
            
        }
        
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            
            switch (identifier) {
                
            case "existing_user to login" :
                
                if let vc = segue.destination as? Login_ViewController {
                    
                    vc.mPhone = mPhone
                    
                }
                
                
            default:
                break
            }

            
        }
        
    }
    

}
