//
//  Swift_ViewController.swift
//  PiDO
//
//  Created by sp developer on 1/30/16.
//  Copyright © 2016 sp developer. All rights reserved.
//

import UIKit

class Swift_ViewController: UIViewController, UITextFieldDelegate {

    var indicator : ActivityIndicator!
    
    @IBOutlet weak var mCustomerID: UITextField!
    @IBOutlet weak var mAmount: UITextField!
    @IBOutlet weak var mPIN: UITextField!
    
    let ProductId = "37"
    let trans_charge = 100
    var CustomerID : NSString = ""
    var PIN : NSString = ""
    var Amount : NSString = ""
    
    var db_MobileNo = ""
    
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo()
            
        }
        
        
        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboar
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(Swift_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == mPIN){
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
            
        else {
            return true
        }
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }
    
    
    func Swift_Task(url_: String){
        
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        //   UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    print("\(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                    
                    self.mCustomerID.text! = ""
                    self.mAmount.text! = ""
                    self.mPIN.text! = ""
                    
                }
                
            }
            
            
        }
        
        task.resume()
            
        
        
    }
    
    
    
    
    func swift_confirmation_Alert(){
        
        let total = trans_charge  +  Amount.integerValue
        
        
        let msg = "Customer ID : \(CustomerID)\n\nAmount : ₦\(Amount)\n\nTransaction Charge : ₦\(trans_charge)\n\nTotal : ₦\(total)"
        
        
        let controller2 = UIAlertController(title: "Transaction Details", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "Dismiss" , style: UIAlertActionStyle.cancel, handler: nil)
        let ActionButton = UIAlertAction(title: "Proceed",style: .destructive, handler: {action in
            
            let encodedCustomerId = UtilHelpers().toBase64(self.CustomerID as String)
            let encodedPIN = UtilHelpers().toBase64(self.PIN as String )
            let encodedMobile = UtilHelpers().toBase64(self.db_MobileNo)
            
            
            let swift_url = Constants.BASE_URL+"method=billspayment&amount=\(self.Amount as String)&customerid=\(encodedCustomerId)&mobile=\(encodedMobile)&pin=\(encodedPIN)&productid=\(self.ProductId)&charge=\(self.trans_charge)"
            
            
            
            if Reachability.isConnectedToNetwork(){ // checks for network connection
                
                self.Swift_Task(url_: swift_url)
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
            
        })
        controller2.addAction(cancelAction)
        controller2.addAction(ActionButton)
        self.present(controller2, animated: true, completion: nil)
        
    }
    
    @IBAction func Swift_Btn(_ sender: UIButton) {
        
        
        mCustomerID.resignFirstResponder()
        mAmount.resignFirstResponder()
        mPIN.resignFirstResponder()
        
        CustomerID = mCustomerID.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        Amount = mAmount.text! as NSString
        PIN  = mPIN.text! as NSString
        
        if(mCustomerID.text!.isEmpty && mAmount.text!.isEmpty && mPIN.text!.isEmpty){
            
            UtilHelpers().displayAlert("ERROR", msg: "Insert the required details into text fields")
        }
            
        else if(CustomerID.length == 0) {
            
            UtilHelpers().displayAlert("ERROR!", msg: "Swift Customer ID Is Required")
        }
            
        else if(PIN.length == 0){
            UtilHelpers().displayAlert("ERROR!", msg: "4 Digit PIN Required")
            
        }
            
        else if(Amount.length == 0){
            
            UtilHelpers().displayAlert("ERROR", msg: "Amount Is Required")
            
        }
            
        else if(PIN.length < 4){
            
            UtilHelpers().displayAlert("ERROR!", msg: "4 Digit PIN Required")
            
        }
            
            
        else {
            
            swift_confirmation_Alert()
            
        }
        
        
    }
}
