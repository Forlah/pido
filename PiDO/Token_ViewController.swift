//
//  Token_ViewController.swift
//  PiDO
//
//  Created by sp developer on 1/11/16.
//  Copyright (c) 2016 sp developer. All rights reserved.
//

import UIKit

class Token_ViewController: UIViewController, UITextFieldDelegate{

    @IBOutlet weak var mToken: UITextField!
    @IBOutlet weak var mPassword: UITextField!
    
    var Token : NSString = ""
    var Password : NSString = ""
    var password_txt = ""
    var surname = ""
    var firstname = ""
    var PiDOMobile = ""
    var UniqueID = ""
    var Email = ""
    var Gender = ""
    var fullname = ""
    
    let AppVersion = "iOS1.0"
    var indicator : ActivityIndicator!
    
    
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboard
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(Token_ViewController.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
        // retreive data from the singleton class
        let data = Reg_Singleton.sharedInstance.getDetails()
        surname = data.getSurname().trimmingCharacters(in: CharacterSet.whitespaces)
        firstname = data.getFirstname().trimmingCharacters(in: CharacterSet.whitespaces)
        password_txt = data.getPassword().trimmingCharacters(in: CharacterSet.whitespaces)
        PiDOMobile = data.getPidoMobile().trimmingCharacters(in: CharacterSet.whitespaces)
        UniqueID = data.getAuthCode()
        Email = data.getEmail().trimmingCharacters(in: CharacterSet.whitespaces)
        Gender = data.getGender().trimmingCharacters(in: CharacterSet.whitespaces)
        
        fullname = "\(surname) \(firstname)"
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }
    
    func Token_task(url_: String){
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let safeurl = URL(string: url_.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
        let request = NSMutableURLRequest(url: safeurl!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error{
                    print("failure \(error)")
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    if (responseStr.contains("8001") == true ){ // successful
                        
                        // retrieve the credential from the singleton class and save user credential to sqlite database
                        
                        let user = User()
                        
                        
                        user.setFullname(self.fullname)
                        user.setMobileNo(self.PiDOMobile)
                        user.setPassword(self.password_txt)
                        user.setUniqueID(self.UniqueID)
                        user.setToken(self.Token as String)
                        user.setGender(self.Gender)
                        user.setEmail(self.Email)
                        
                        // create and save to database
                        let pidouser_db = PiDOUser_DB()
                        
                        pidouser_db.createTable()
                        
                        if pidouser_db.savePiDOUser(user) == 1 {
                            
                            self.indicator.stopAnimating()
                            
                            // segue to login viewcontroller
                            self.performSegue(withIdentifier: "Token_to_login", sender: self)
                        }
                        
                        
                    }
                        
                    else if(responseStr.contains("8000")){
                        
                        self.indicator.stopAnimating()
                        
                        
                        UtilHelpers().displayAlert("RESPONSE", msg: "Connection Error Please Try again")
                        
                    }
                        
                    else {
                        
                        self.indicator.stopAnimating()
                        
                        
                        UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                        
                    }
                    
                    
                }
                
            }
            
           

        }
        task.resume()
        
        
    }
    
    
    
    
    @IBAction func Token_Btn(_ sender: UIButton) {
        
       Token =  mToken.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
       Password = mPassword.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        
        mToken.resignFirstResponder()
        mPassword.resignFirstResponder()
        
        
        if(mToken.text!.isEmpty && mPassword.text!.isEmpty){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Insert the required details into text fields")
            
        }
        
        else if(Token.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Token Number Is Required")
            
            
        }
        
        
        else if(Password.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Password Is Required")
            
        }
        
        else if(Password.isEqual(to: password_txt) == false){
            UtilHelpers().displayAlert("ERROR!", msg: "Password Does Not Match Your Initial Password")
        }
        
        
        else {
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                let encodedMobileNo = UtilHelpers().toBase64(PiDOMobile)
                let encodedUniqueID = UtilHelpers().toBase64(UniqueID)
                
              let token_login_uri = Constants.BASE_URL+"method=login&msisdn=\(encodedMobileNo)&password=\(password_txt)&autcode=\(encodedUniqueID)&device=iOS&token=\(Token as String)&version=\(AppVersion)"
                
                  Token_task(url_: token_login_uri)

                
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }
            
        }
        
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
