//
//  TransHistory_ViewControllerTableViewController.swift
//  PiDO
//
//  Created by sp developer on 2/4/16.
//  Copyright © 2016 sp developer. All rights reserved.
//

import UIKit

class TransHistory_ViewControllerTableViewController: UITableViewController, UITextFieldDelegate {
    
    
//    let contentString = "s/n | Acc | trans_no | Date | Type | Desc | TransId |, s/n1 | Acc1 | trans_no1 | Date1 | Type1 | Desc1 | TransId1 |,s/n2 | Acc2 | trans_no2 | Date2 | Type2 | Desc2 | TransId2 |,s/n3 | Acc3 | trans_no3 | Date3 | Type3 | Desc3 | TransId3 |, s/n4 | Acc4 | trans_no4 | Date4 | Type4 | Desc4 | TransId4 "
    
    var string_length = [String]()
    var history : [String] = []
    
    var mPIN : UITextField!
    
    
    var PIN = ""
    var db_MobileNo : NSString = ""
    
    var indicator : ActivityIndicator!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if(PiDOUser_DB().isExisting() == true){
            
            let db_Reg = PiDOUser_DB().getAll()
            
            db_MobileNo = db_Reg[0].getMobileNo() as NSString
            
            
        }
        
        
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        promptForAnswer()
        
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == mPIN {
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
            
        else{
            return false
        }
    }
    
    
    func History_Task(url_: String){
        indicator = ActivityIndicator(title: "Processing...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        
        var responseStr: NSString = ""
        let request = NSMutableURLRequest(url: URL(string: url_)!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    print( (error))
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    self.indicator.stopAnimating()
                    
                    if(responseStr.contains(",")){
                        
                        self.string_length = responseStr.components(separatedBy: ",")
                        
                        for i in 0..<self.string_length.count{
                            self.history.append(self.string_length[i])
                        }
                        
                        self.tableView.reloadData()
                    }
                        
                        
                    else {
                        
                        UtilHelpers().displayAlert("Response", msg: responseStr as String)
                    }
                    
                    
                    
                }
                
            }
            
           
        }
        task.resume()
        
        
        
    }
    
    
    func promptForAnswer() {
        let ac = UIAlertController(title: "Please Enter Your PIN", message: nil, preferredStyle: .alert)
        ac.addTextField(configurationHandler: nil)
        mPIN = ac.textFields![0]
        mPIN.keyboardType = UIKeyboardType.numberPad
        mPIN.delegate = self
        mPIN.isSecureTextEntry = true
        mPIN.clearButtonMode = UITextFieldViewMode.whileEditing
        
        let submitAction = UIAlertAction(title: "Submit", style: .default) { (action : UIAlertAction) -> Void in
            
            self.PIN = self.mPIN.text!.trimmingCharacters(in: CharacterSet.whitespaces)
            
            
            let Text_msg = self.PIN as NSString
            
            if (Text_msg.length == 0){
                
                UtilHelpers().displayAlert("ERROR", msg: "4 Digit PIN Required")
                
            }
                
            else if(Text_msg.length < 4){
                
                UtilHelpers().displayAlert("ERROR", msg: "4 Digit PIN Required")
                
            }
            
            else{
                
                
                if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                    
                    
                    let encodedPIN = UtilHelpers().toBase64(self.PIN)
                    let subString = self.db_MobileNo.substring(from: 1)
                  
                    let history_url = Constants.BASE_URL+"method=transactionhistory&MobileNumber=\(subString)&UserPassword=\(encodedPIN)"
                    
                    self.History_Task(url_: history_url)
                }
                    
                else {
                    
                    UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                    
                }
                
            }

        }
        
        
        ac.addAction(submitAction)
        
        present(ac, animated: true, completion: nil)
    }


   
    // MARK: - Table view data source

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return string_length.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "history_cells", for: indexPath) as! TableViewCellAdapter

        let rowData = history[indexPath.row]
        
        print("The row Data is\(rowData)")
        
        if(rowData.contains("|")){
            
            var outarray = rowData.components(separatedBy: "|")
            
            var rowItem  : [String?] = []
            
//            for(var i : Int = 0 ; i < outarray.count ; i++){
//                rowItem.append(outarray[i])
//                print("Out array list\(indexPath.row) = \(rowItem[i])\n")
//                
//            }
            
            for i in 0..<outarray.count{
                
                rowItem.append(outarray[i])
                print("Out array list\(indexPath.row) = \(String(describing: rowItem[i]))\n")
                

            }
            
            cell.acc.text = rowItem[0]
            cell.amount.text = rowItem[1]
            cell.date.text = rowItem[2]
            cell.trans_type.text = rowItem[3]
            cell.type.text = rowItem[4]
            cell.desc.text = rowItem[5]
            cell.Trans_id.text = rowItem[6]
            
            
            
            return cell
            
        }
        
        else{
            
            return cell
        }

        
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
