//
//  User.swift
//  PiDO
//
//  Created by sp developer on 1/9/16.
//  Copyright (c) 2016 sp developer. All rights reserved.
//

import Foundation

class User {
    
    var id : Int = -1
    var fullname : String = ""
    var mobile_no : String = ""
    var password : String = ""
    var uniqueId : String = ""
    var token : String = ""
    var email : String = ""
    var gender : String = ""
    
    // empty constructor
    init() {
        
    }
    
    
    func getid() -> Int {
        
        return id
    }
    
    func setid(_ _id : Int) {
        id = _id
    }
    
    func getFullname() -> String {
        
        return fullname
    }
    
    func setFullname(_ _fullname : String) {
        fullname = _fullname
    }
    
    func getMobileNo() -> String {
        
        return mobile_no
    }
    
    func setMobileNo(_ _mobileno : String) {
        
        mobile_no = _mobileno
    }
    
    func getPassword() -> String {
        
        return password
    }
    
    func setPassword(_ _password : String) {
        
        password = _password
    }
    
    func getUniqueID() -> String {
        
        return uniqueId
    }
    
    func setUniqueID(_ _uniqueId : String) {
        
        uniqueId = _uniqueId
    }
    
    
    func getToken() -> String {
        
        return token
    }
    
    func setToken(_ _token : String) {
        
        token = _token
        
    }
    
    func getEmail() -> String {
        
        return email
    }
    
    func setEmail(_ _email : String) {
        
        email = _email
        
    }
    
    func getGender() -> String {
        
        return gender
    }
    
    func setGender(_ _gender : String) {
        gender = _gender
    }
    
    
    
    
    
    
    
    
    

}
