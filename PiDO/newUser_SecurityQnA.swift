//
//  newUser_SecurityQnA.swift
//  PiDO
//
//  Created by sp developer on 12/28/15.
//  Copyright (c) 2015 sp developer. All rights reserved.
//

import UIKit

class newUser_SecurityQnA: UIViewController, UITextFieldDelegate {
    
    var SecurityQuestion : NSString = ""
    var SecurityAnswer : NSString = ""
    var ConfirmAnswer : NSString = ""

    @IBOutlet weak var mSecurityQuestion: UITextField!
    @IBOutlet weak var mSecurityAnswer: UITextField!
    
    @IBOutlet weak var mConfirmAnswer: UITextField!
    
    var indicator : ActivityIndicator!
    
    
    let AppVersion = "iOS1.0"
    let device = "iOS"
    var UniqueID : String = ""
    
    var firstname = ""
    var surname = ""
    var PiDOMobile_No = ""
    var Password = ""
    var Token = ""
    var Gender = ""
    var Email = ""
    
    
    // dismiss keyboard
    func didTapView(){
        self.view.endEditing(true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // when a blank spot on the screen is pressed, tells any textfield to resign from first responder role therefore dismissing the keyboard
        
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(newUser_SecurityQnA.didTapView))
        self.view.addGestureRecognizer(tapRecogniser)
        
        
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         textField.resignFirstResponder()
        
        return false
    }
    
    func getToken_task(url_: String){
        
        indicator = ActivityIndicator(title: "Please Wait...", center: self.view.center)
        self.view.addSubview(indicator.getViewActivityIndicator())
        
        self.indicator.startAnimating()
        var responseStr: NSString = ""
        let safeUrl = URL(string: url_.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
        let request = NSMutableURLRequest(url: safeUrl!)
        request.timeoutInterval = Constants.TIMEOUT_INTERVAL
        request.httpMethod = "POST"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest){ data, response, error in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    print( error)
                    
                    responseStr = "Unable to connect to the service at the moment, Please try again later"
                    self.indicator.stopAnimating()
                    
                    UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                    
                }
                    
                else {
                    
                    print("\(data!.count) bytes of data was returned")
                    
                    responseStr  = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!
                    
                    self.indicator.stopAnimating()
                    
                    
                    print("Response from service : \(responseStr)")
                    
                    if (responseStr.contains("5000") == true ){
                        
                        UtilHelpers().displayAlert("RESPONSE", msg: "Phone Number Has Been Registered By Another Subscriber")
                        
                    }
                        
                    else if(responseStr.contains("6000")){
                        
                        // display a success dialog before segue to login viewcontroller
                        
                        
                        let controller2 = UIAlertController(title: "RESPONSE", message: "Registration Successful. Your token will be sent shortly..", preferredStyle: UIAlertControllerStyle.alert)
                        
                        let ActionButton = UIAlertAction(title: "OK",style: .default, handler: { action in
                            
                            self.performSegue(withIdentifier: "new_user to token", sender: self)
                            
                        })
                        
                        controller2.addAction(ActionButton)
                        self.present(controller2, animated: true, completion: nil)
                        
                        
                    }
                        
                    else if(responseStr.contains("7000")){
                        UtilHelpers().displayAlert("RESPONSE", msg: " Server Internal Error")
                        
                    }
                        
                    else {
                        
                        UtilHelpers().displayAlert("RESPONSE", msg: responseStr as String)
                        
                    }
                    
                }
                
            }
            
           
        
        }
        task.resume()
        
    }
    

    @IBAction func newUser_Btn(_ sender: UIButton) {
        
        mSecurityQuestion.resignFirstResponder()
        mSecurityAnswer.resignFirstResponder()
        mConfirmAnswer.resignFirstResponder()
        
       SecurityQuestion =  mSecurityQuestion.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        
       SecurityAnswer = mSecurityAnswer.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        
        
        ConfirmAnswer = mConfirmAnswer.text!.trimmingCharacters(in: CharacterSet.whitespaces) as NSString
        
        if(mSecurityQuestion.text!.isEmpty && mSecurityAnswer.text!.isEmpty && mConfirmAnswer.text!.isEmpty){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Insert the required details into text fields")
            
        }
        
        else if(SecurityQuestion.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Security Question Is Required")
            
        }
        
        else if(SecurityAnswer.length == 0){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Security Answer Is Required")
            
        }
        
        else if(ConfirmAnswer.isEqual(SecurityAnswer) == false){
            
            UtilHelpers().displayAlert("ERROR!", msg: "Security Answers Do Not Match")
            
        }
        
        else {
            
            let data = Reg_Singleton.sharedInstance.getDetails()
            
           firstname =  data.getFirstname().trimmingCharacters(in: CharacterSet.whitespaces)
           surname =  data.getSurname().trimmingCharacters(in: CharacterSet.whitespaces)
           PiDOMobile_No =  data.getPidoMobile()
           Password =  data.getPassword()
           UniqueID =  data.getAuthCode()
           Token =  data.getToken()
           Gender = data.getGender()
           Email =  data.getEmail()
            
            let encodedMobile = UtilHelpers().toBase64(PiDOMobile_No)
            let encodedPassword = UtilHelpers().toBase64(Password)
            let encodedUniqueID = UtilHelpers().toBase64(UniqueID)
            let encodedSQuestion = UtilHelpers().toBase64(SecurityQuestion as String)
            let encodedSAnswer = UtilHelpers().toBase64(SecurityAnswer as String)
            
            let reg_user_url = Constants.BASE_URL+"method=register1&firstname=\(firstname)&surname=\(surname)&msisdn=\(encodedMobile)&email=\(Email)&password=\(encodedPassword)&version=\(AppVersion)&device=\(device)&haveaccount=yes&autcode=\(encodedUniqueID)&securityquestion=\(encodedSQuestion)&securityanswer=\(encodedSAnswer)"
            
            
            if Reachability.isConnectedToNetwork(){ // checks for network connectivity
                
                getToken_task(url_: reg_user_url)
                
//                mSecurityQuestion.text = ""
//                mSecurityAnswer.text = ""
//                mConfirmAnswer.text = ""
                
            }
                
            else {
                
                UtilHelpers().displayAlert("No Internet Connection", msg: "Make Sure your device is connected to the internet")
                
            }

            
            
            
        }

        
       
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
